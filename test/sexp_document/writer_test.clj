(ns sexp-document.writer-test
  (:require [clojure.test :refer :all])
  (:require [sexp-document.document-reader :as doc])
  (:use [sexp-document.writer])
  (:require [clojure.pprint :as pprint])
  )

(def document (doc/doc-tree->map (doc/doc-str->tree (str '(root
                                                            (node1 {:att1 1 :att2 2}
                                                                   (node2 "1")
                                                                   (node3 "value1"))
                                                            (node4 "value2"))))))

(deftest doc-tree->map-test
  (is (= (doc-map->tree document)
         '("root"
            ("node1" {:att2 "2", :att1 "1"} ("node2" "1") ("node3" "value1"))
            ("node4" "value2")))))


;(print (ppxml (document-to-xml {:tag :root,
;                     :attrs          {:att1 "1" :att2 "2"}
;                     :content
;                                     [{:tag :node1,
;                              :content
;                                   [{:tag :node2, :content ["1"]}
;                                    {:tag :node3, :content ["value1"]}]}
;                             {:tag :node4, :content ["value2"]}]})))

(run-tests 'sexp-document.writer-test)
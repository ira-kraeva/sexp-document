(ns sexp-document.path-test
  (:require [clojure.test :refer :all])
  (:use [sexp-document.path-reader])
  (:require [clojure.pprint :as pprint])
  (:import (clojure.lang ExceptionInfo)))

(deftest a-r-path-test
  (is (= (a-r-path '(:path
                      (:absolutePath
                        "/"
                        (:step (:nodeTest "root"))
                        (:relativePath
                          "//"
                          (:step (:nodeTest "*"))))))
         '(:absolutePath
            "/"
            (:step (:nodeTest "root"))
            (:relativePath
              "//"
              (:step (:nodeTest "*"))))))

  (is (= (a-r-path (a-r-path (path-str->tree "/root")))
         nil)))


(deftest step-test
  (is (= (step '(:absolutePath
                  "/"
                  (:step (:nodeTest "root"))
                  (:relativePath
                    "//"
                    (:step (:nodeTest "node1"))
                    (:absolutePath "/" (:step (:nodeTest "node2"))))))
         '(:nodeTest "root")))
  (is (= (step '(:absolutePath
                  "/"
                  (:relativePath
                    "//"
                    (:step (:nodeTest "node1"))
                    (:absolutePath "/" (:step (:nodeTest "node2"))))))
         nil)))


(deftest test-node-test
  (is (= (test-node '(:absolutePath
                       "/"
                       (:step (:nodeTest "root"))
                       (:relativePath
                         "//"
                         (:step (:nodeTest "node1"))
                         (:absolutePath "/" (:step (:nodeTest "node2"))))))
         "root"))
  (is (= (test-node '(:absolutePath
                       "/"
                       (:step (:nodeTest "*"))
                       (:relativePath
                         "//"
                         (:step (:nodeTest "*"))
                         (:absolutePath "/" (:step (:nodeTest "node2"))))))
         "*")))


(deftest predicate-test
  (is (= (predicate '(:absolutePath
                       "/"
                       (:step
                         (:nodeTest "node3")
                         (:predicate
                           "["
                           (:expression
                             (:attribute "@" "att")
                             (:operator ">")
                             (:value "10"))
                           "]"))))
         '((:expression (:attribute "@" "att") (:operator ">") (:value "10")))))

  (is (empty? (predicate '(:predicate
                            "["
                            (:expression
                              (:attribute "@" "att")
                              (:operator ">")
                              (:value "10"))
                            "]")))))


(deftest expression-test
  (is (= (expression '(:predicate
                        "["
                        (:expression
                          (:attribute "@" "att")
                          (:operator ">")
                          (:value "10"))
                        "]"))
         '((:attribute "@" "att") (:operator ">") (:value "10"))))

  (is (empty? (expression '(:predicate
                             "["
                             "]")))))


(deftest function-test
  (is (= (function '((:function "text" "()")
                     (:operator ">")
                     (:value "10")))
         "text"))

  (is (nil? (expression '((:attribute "@" "att")
                          (:operator ">")
                          (:value "10"))))))

(deftest operator-test
  (is (= (operator '((:function "text" "()")
                     (:operator ">")
                     (:value "10")))
         ">"))

  (is (nil? (operator '((:function "@" "att")
                        (:value "10"))))))

(deftest value-test
  (is (= (value '((:function "text" "()")
                  (:operator ">")
                  (:value "10")))
         "10"))

  (is (nil? (value '((:function "@" "att")
                     (:operator ">"))))))

(deftest attribute-test
  (is (= (attribute '((:attribute "@" "att")
                      (:operator ">")
                      (:value "10")))
         "att"))

  (is (nil? (value '((:function "@" "att")
                     (:operator ">"))))))

(deftest next-step-test
  (is (= (next-step '(:path
                      (:absolutePath
                        "/"
                        (:step (:nodeTest "root"))
                        (:relativePath
                          "//"
                          (:step (:nodeTest "*"))))))
         '(:relativePath
            "//"
            (:step (:nodeTest "*")))))

  (is (= (a-r-path '(:absolutePath
                     "/"
                     (:step (:nodeTest "root"))
                     (:relativePath
                       "//"
                       (:step (:nodeTest "*")))))
         '(:relativePath
           "//"
           (:step (:nodeTest "*")))))

  (is (= (a-r-path '(:relativePath
                      "//"
                      (:step (:nodeTest "*"))))
         nil)))


(deftest path-str->tree-test
  (is (= (path-str->tree "/*//node1/node2//*/node3//node4/*/node5/*")
         '(:path
            (:absolutePath
              "/"
              (:step (:nodeTest "*"))
              (:relativePath
                "//"
                (:step (:nodeTest "node1"))
                (:absolutePath
                  "/"
                  (:step (:nodeTest "node2"))
                  (:relativePath
                    "//"
                    (:step (:nodeTest "*"))
                    (:absolutePath
                      "/"
                      (:step (:nodeTest "node3"))
                      (:relativePath
                        "//"
                        (:step (:nodeTest "node4"))
                        (:absolutePath
                          "/"
                          (:step (:nodeTest "*"))
                          (:absolutePath
                            "/"
                            (:step (:nodeTest "node5"))
                            (:absolutePath "/" (:step (:nodeTest "*")))))))))))))))


(deftest path-tree->map-test
  (is (= (path-tree->map (path-str->tree "/*//node1/node2//*[test() = 10]/node3[@att > 10]//node4/*/node5/*"))
         '{:context "child",
           :node-test "*",
           :predicate {:function nil, :attribute nil, :operator nil, :value nil},
           :next-step
           {:context "descendant-or-self",
            :node-test "node1",
            :predicate
            {:function nil, :attribute nil, :operator nil, :value nil},
            :next-step
            {:context "child",
             :node-test "node2",
             :predicate
             {:function nil, :attribute nil, :operator nil, :value nil},
             :next-step
             {:context "descendant-or-self",
              :node-test "*",
              :predicate
              {:function "test", :attribute nil, :operator "=", :value "10"},
              :next-step
              {:context "child",
               :node-test "node3",
               :predicate
               {:function nil, :attribute "att", :operator ">", :value "10"},
               :next-step
               {:context "descendant-or-self",
                :node-test "node4",
                :predicate
                {:function nil, :attribute nil, :operator nil, :value nil},
                :next-step
                {:context "child",
                 :node-test "*",
                 :predicate
                 {:function nil, :attribute nil, :operator nil, :value nil},
                 :next-step
                 {:context "child",
                  :node-test "node5",
                  :predicate
                  {:function nil, :attribute nil, :operator nil, :value nil},
                  :next-step
                  {:context "child",
                   :node-test "*",
                   :predicate
                   {:function nil,
                    :attribute nil,
                    :operator nil,
                    :value nil}}}}}}}}}})))

(run-tests 'sexp-document.path-test)

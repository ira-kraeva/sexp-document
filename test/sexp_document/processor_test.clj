(ns sexp-document.processor-test
  (:require [clojure.test :refer :all])
  (:use [sexp-document.processor])
  (:require [sexp-document.document-reader :as doc])
  (:require [sexp-document.schema-reader :as schema])
  (:require [sexp-document.path-reader :as path])
  (:require [clojure.pprint :as pprint]
            [sexp-document.stylesheet-reader :as stylesheet])
  (:import (clojure.lang ExceptionInfo)))

(deftest node-test-check-test
  (is (true? (node-test-check
               '{:node "node4",
                 :node4
                       {:text "value2", :attributes {}, :path (:node4 :2 :children :root)}}

               '{:context   "descendant-or-self",
                 :node-test "node4",
                 :predicate
                            {:selector  nil,
                             :function  nil,
                             :attribute nil,
                             :operator  nil,
                             :value     nil}})))

  (is (true? (node-test-check
               '{:node "node4",
                 :node4
                       {:text "value2", :attributes {}, :path (:node4 :2 :children :root)}}

               '{:context   "descendant-or-self",
                 :node-test "*",
                 :predicate
                            {:selector  nil,
                             :function  nil,
                             :attribute nil,
                             :operator  nil,
                             :value     nil}})))

  (is (false? (node-test-check
                '{:context   "descendant-or-self",
                  :node-test "node4",
                  :predicate
                             {:selector  nil,
                              :function  nil,
                              :attribute nil,
                              :operator  nil,
                              :value     nil}}
                '{:node "node5",
                  :node4
                        {:text "value2", :attributes {}, :path (:node4 :2 :children :root)}}))))

(deftest parse-number-test
  (is (= (parse-number "12345")
         12345)))

(deftest number?-test
  (is (true? (is-number? "12345")))
  (is (false? (is-number? "q12345"))))

(deftest compare-value-test
  (is (true? (compare-value "=" "12345" "12345")))
  (is (false? (compare-value "=" "12345" "12346")))
  (is (false? (compare-value "=" "value1" "value2")))
  (is (true? (compare-value "=" "value1" "value1")))
  (is (true? (compare-value ">" "12346" "12345")))
  (is (false? (compare-value ">" "12344" "12345")))
  (is (false? (compare-value ">" "12345" "value1")))
  (is (true? (compare-value "<" "12344" "12345")))
  (is (false? (compare-value "<" "12345" "12344")))
  (is (false? (compare-value "<" "12345" "value1"))))


(deftest predicate-check?-test
  (is (true? (predicate-check
               '{:context   "descendant-or-self",
                 :node-test "node4",
                 :predicate
                            {:function  "text",
                             :attribute nil,
                             :operator  "=",
                             :value     "value2"}}
               '{:node "node4",
                 :node4
                       {:text "value2", :attributes {}, :path (:node4 :2 :children :root)}})))

  (is (false? (predicate-check
                '{:context   "descendant-or-self",
                  :node-test "node4",
                  :predicate
                             {:function  "text",
                              :attribute nil,
                              :operator  "=",
                              :value     "value2"}}
                '{:node "node4",
                  :node4
                        {:text "value3", :attributes {}, :path (:node4 :2 :children :root)}})))

  (is (thrown? ExceptionInfo (predicate-check
                               '{:context   "descendant-or-self",
                                 :node-test "node4",
                                 :predicate
                                            {:function  "not-text",
                                             :attribute nil,
                                             :operator  ">",
                                             :value     "10"}}
                               '{:node "node4",
                                 :node4
                                       {:text "11", :attributes {}, :path (:node4 :2 :children :root)}})))

  (is (true? (predicate-check
               '{:context   "descendant-or-self",
                 :node-test "node4",
                 :predicate
                            {:function  nil,
                             :attribute "att1",
                             :operator  "=",
                             :value     "10"}}
               '{:node "node4",
                 :node4
                       {:text "9", :attributes {:att1 "10" :att2 "value1"}, :path (:node4 :2 :children :root)}})))

  (is (false? (predicate-check
                '{:context   "descendant-or-self",
                  :node-test "node4",
                  :predicate
                             {:function  nil,
                              :attribute "att2",
                              :operator  ">",
                              :value     "10"}}
                '{:node "node4",
                  :node4
                        {:text "9", :attributes {:att1 "10" :att2 "value1"}, :path (:node4 :2 :children :root)}}))))

(def doc-map (doc/doc-tree->map (doc/doc-str->tree (str '(root
                                                           (node1 {:att1 1 :att2 2}
                                                                  (node2 "1")
                                                                  (node3 "value1"))
                                                           (node4 "value2"))))))

(deftest descendant-nodes-test
  (is (= (descendant-nodes '{:node "node1",
                             :node1
                                   {:text nil,
                                    :path (:node1 :1 :children :root :1 :children :document),
                                    :children
                                          {:1
                                           {:node "node2",
                                            :node2
                                                  {:text "1",
                                                   :path
                                                         (:node2
                                                           :1
                                                           :children
                                                           :node1
                                                           :1
                                                           :children
                                                           :root
                                                           :1
                                                           :children
                                                           :document)}},
                                           :2
                                           {:node "node3",
                                            :node3
                                                  {:text "value1",
                                                   :path
                                                         (:node3
                                                           :2
                                                           :children
                                                           :node1
                                                           :1
                                                           :children
                                                           :root
                                                           :1
                                                           :children
                                                           :document)}}}}})
         '({:node "node2",
            :node2
                  {:text "1",
                   :path
                         (:node2
                           :1
                           :children
                           :node1
                           :1
                           :children
                           :root
                           :1
                           :children
                           :document)}}
           {:node "node3",
            :node3
                  {:text "value1",
                   :path
                         (:node3
                           :2
                           :children
                           :node1
                           :1
                           :children
                           :root
                           :1
                           :children
                           :document)}})))

  (is (= (descendant-nodes '{:node "node2",
                             :node2
                                   {:text "1",
                                    :path
                                          (:node2
                                            :1
                                            :children
                                            :node1
                                            :1
                                            :children
                                            :root
                                            :1
                                            :children
                                            :document)}})
         ())))


(deftest descendant-self-nodes-test
  (is (= (descendant-self-nodes '{:node "node1",
                                  :node1
                                        {:text nil,
                                         :path (:node1 :1 :children :root :1 :children :document),
                                         :children
                                               {:1
                                                {:node "node2",
                                                 :node2
                                                       {:text "1",
                                                        :path
                                                              (:node2
                                                                :1
                                                                :children
                                                                :node1
                                                                :1
                                                                :children
                                                                :root
                                                                :1
                                                                :children
                                                                :document)}},
                                                :2
                                                {:node "node3",
                                                 :node3
                                                       {:text "value1",
                                                        :path
                                                              (:node3
                                                                :2
                                                                :children
                                                                :node1
                                                                :1
                                                                :children
                                                                :root
                                                                :1
                                                                :children
                                                                :document)}}}}})
         '({:node "node1",
            :node1
                  {:text nil,
                   :path (:node1 :1 :children :root :1 :children :document),
                   :children
                         {:1
                          {:node "node2",
                           :node2
                                 {:text "1",
                                  :path
                                        (:node2
                                          :1
                                          :children
                                          :node1
                                          :1
                                          :children
                                          :root
                                          :1
                                          :children
                                          :document)}},
                          :2
                          {:node "node3",
                           :node3
                                 {:text "value1",
                                  :path
                                        (:node3
                                          :2
                                          :children
                                          :node1
                                          :1
                                          :children
                                          :root
                                          :1
                                          :children
                                          :document)}}}}}
           {:node "node2",
            :node2
                  {:text "1",
                   :path
                         (:node2
                           :1
                           :children
                           :node1
                           :1
                           :children
                           :root
                           :1
                           :children
                           :document)}}
           {:node "node3",
            :node3
                  {:text "value1",
                   :path
                         (:node3
                           :2
                           :children
                           :node1
                           :1
                           :children
                           :root
                           :1
                           :children
                           :document)}})))

  (is (= (descendant-self-nodes '{:node "node2",
                                  :node2
                                        {:text "1",
                                         :path
                                               (:node2
                                                 :1
                                                 :children
                                                 :node1
                                                 :1
                                                 :children
                                                 :root
                                                 :1
                                                 :children
                                                 :document)}})
         '({:node "node2",
            :node2
                  {:text "1",
                   :path
                         (:node2
                           :1
                           :children
                           :node1
                           :1
                           :children
                           :root
                           :1
                           :children
                           :document)}}))))

(def path (fn [str] (path/path-tree->map (path/path-str->tree str))))

;(deftest compare-xpath-node-test
;  (is (= (compare-xpath-node (path "/root") doc-map)
;         '({:node "root",
;            :root
;                  {:text       nil,
;                   :attributes {},
;                   :path       (:root),
;                   :children
;                               {:1
;                                {:node "node1",
;                                 :node1
;                                       {:text       nil,
;                                        :attributes {:att2 "2", :att1 "1"},
;                                        :path       (:node1 :1 :children :root),
;                                        :children
;                                                    {:1
;                                                     {:node "node2",
;                                                      :node2
;                                                            {:text       "1",
;                                                             :attributes {},
;                                                             :path       (:node2 :1 :children :node1 :1 :children :root)}},
;                                                     :2
;                                                     {:node "node3",
;                                                      :node3
;                                                            {:text       "value1",
;                                                             :attributes {},
;                                                             :path       (:node3 :2 :children :node1 :1 :children :root)}}}}},
;                                :2
;                                {:node "node4",
;                                 :node4
;                                       {:text       "value2",
;                                        :attributes {},
;                                        :path       (:node4 :2 :children :root)}}}}})))
;
;  (is (not= (compare-xpath-node (path "/root/node1") doc-map)
;            '{:node "root",
;              :root
;                    {:text       nil,
;                     :attributes {},
;                     :path       (:root),
;                     :children
;                                 {:1
;                                  {:node "node1",
;                                   :node1
;                                         {:text       nil,
;                                          :attributes {:att2 "2", :att1 "1"},
;                                          :path       (:node1 :1 :children :root),
;                                          :children
;                                                      {:1
;                                                       {:node "node2",
;                                                        :node2
;                                                              {:text       "1",
;                                                               :attributes {},
;                                                               :path       (:node2 :1 :children :node1 :1 :children :root)}},
;                                                       :2
;                                                       {:node "node3",
;                                                        :node3
;                                                              {:text       "value1",
;                                                               :attributes {},
;                                                               :path       (:node3 :2 :children :node1 :1 :children :root)}}}}},
;                                  :2
;                                  {:node "node4",
;                                   :node4
;                                         {:text       "value2",
;                                          :attributes {},
;                                          :path       (:node4 :2 :children :root)}}}}}))
;
;  (is (= (compare-xpath-node (path "//*") doc-map)
;         '({:node "root",
;            :root
;                  {:text       nil,
;                   :attributes {},
;                   :path       (:root),
;                   :children
;                               {:1
;                                {:node "node1",
;                                 :node1
;                                       {:text       nil,
;                                        :attributes {:att2 "2", :att1 "1"},
;                                        :path       (:node1 :1 :children :root),
;                                        :children
;                                                    {:1
;                                                     {:node "node2",
;                                                      :node2
;                                                            {:text       "1",
;                                                             :attributes {},
;                                                             :path       (:node2 :1 :children :node1 :1 :children :root)}},
;                                                     :2
;                                                     {:node "node3",
;                                                      :node3
;                                                            {:text       "value1",
;                                                             :attributes {},
;                                                             :path       (:node3 :2 :children :node1 :1 :children :root)}}}}},
;                                :2
;                                {:node "node4",
;                                 :node4
;                                       {:text       "value2",
;                                        :attributes {},
;                                        :path       (:node4 :2 :children :root)}}}}}
;           {:node "node1",
;            :node1
;                  {:text       nil,
;                   :attributes {:att2 "2", :att1 "1"},
;                   :path       (:node1 :1 :children :root),
;                   :children
;                               {:1
;                                {:node "node2",
;                                 :node2
;                                       {:text       "1",
;                                        :attributes {},
;                                        :path       (:node2 :1 :children :node1 :1 :children :root)}},
;                                :2
;                                {:node "node3",
;                                 :node3
;                                       {:text       "value1",
;                                        :attributes {},
;                                        :path       (:node3 :2 :children :node1 :1 :children :root)}}}}}
;           {:node "node2",
;            :node2
;                  {:text       "1",
;                   :attributes {},
;                   :path       (:node2 :1 :children :node1 :1 :children :root)}}
;           {:node "node3",
;            :node3
;                  {:text       "value1",
;                   :attributes {},
;                   :path       (:node3 :2 :children :node1 :1 :children :root)}}
;           {:node "node4",
;            :node4
;                  {:text "value2", :attributes {}, :path (:node4 :2 :children :root)}}))))


(deftest name-check-test
  (is (true? (name-check
               '{:element      "node3",
                 :type         "string",
                 :restrictions ("equal" "value2")}

               '{:node "node3",
                 :node3
                       {:text "value1",
                        :path
                              (:node3
                                :2
                                :children
                                :node1
                                :1
                                :children
                                :root
                                :1
                                :children
                                :document)}})))

  (is (thrown? ExceptionInfo
               (name-check
                 '{:element      "node3",
                   :type         "string",
                   :restrictions ("equal" "value2")}

                 '{:node "node4",
                   :node3
                         {:text "value1",
                          :path
                                (:node3
                                  :2
                                  :children
                                  :node1
                                  :1
                                  :children
                                  :root
                                  :1
                                  :children
                                  :document)}}))))


(deftest type-check-test
  (is (true? (type-check
               '{:element      "node3",
                 :type         "string",
                 :restrictions ("equal" "value2")}

               '{:node "node3",
                 :node3
                       {:text "value1",
                        :path
                              (:node3
                                :2
                                :children
                                :node1
                                :1
                                :children
                                :root
                                :1
                                :children
                                :document)}})))

  (is (thrown? ExceptionInfo
               (type-check
                 '{:element      "node3",
                   :type         "string",
                   :restrictions ("equal" "value2")}

                 '{:node "node4",
                   :node3
                         {:text "123",
                          :path
                                (:node3
                                  :2
                                  :children
                                  :node1
                                  :1
                                  :children
                                  :root
                                  :1
                                  :children
                                  :document)}}))))


(deftest children-check-test
  ;(is (true? (children-check
  ;             '{:element      "node1",
  ;               :type         nil,
  ;               :restrictions (),
  ;               :complexType
  ;                             {:name nil,
  ;                              :elements
  ;                                    ({:element      "node2",
  ;                                      :type         "number",
  ;                                      :restrictions ("min-value" "0" "max-value" "10")}
  ;                                     {:element      "node3",
  ;                                      :type         "string",
  ;                                      :restrictions ("equal" "value2")})}}
  ;
  ;             '{:node "node1",
  ;               :node1
  ;                     {:text       nil,
  ;                      :attributes {:att2 "2", :att1 "1"},
  ;                      :path       (:node1 :1 :children :root),
  ;                      :children
  ;                                  {:1
  ;                                   {:node "node2",
  ;                                    :node2
  ;                                          {:text       "1",
  ;                                           :attributes {},
  ;                                           :path       (:node2 :1 :children :node1 :1 :children :root)}},
  ;                                   :2
  ;                                   {:node "node3",
  ;                                    :node3
  ;                                          {:text       "value1",
  ;                                           :attributes {},
  ;                                           :path       (:node3 :2 :children :node1 :1 :children :root)}}}}}
  ;             ())))

  ;(is (thrown? ExceptionInfo
  ;             (children-check
  ;               '{:element      "node1",
  ;                 :type         nil,
  ;                 :restrictions ()}
  ;
  ;               '{:node "node1",
  ;                 :node1
  ;                       {:text       nil,
  ;                        :attributes {:att2 "2", :att1 "1"},
  ;                        :path       (:node1 :1 :children :root),
  ;                        :children
  ;                                    {:1
  ;                                     {:node "node2",
  ;                                      :node2
  ;                                            {:text       "1",
  ;                                             :attributes {},
  ;                                             :path       (:node2 :1 :children :node1 :1 :children :root)}},
  ;                                     :2
  ;                                     {:node "node3",
  ;                                      :node3
  ;                                            {:text       "value1",
  ;                                             :attributes {},
  ;                                             :path       (:node3 :2 :children :node1 :1 :children :root)}}}}}
  ;               ())))

  ;(is (thrown? ExceptionInfo
  ;             (children-check
  ;               '{:element      "node1",
  ;                 :type         nil,
  ;                 :restrictions (),
  ;                 :complexType
  ;                               {:name nil,
  ;                                :elements
  ;                                      ({:element      "node2",
  ;                                        :type         "number",
  ;                                        :restrictions ("min-value" "0" "max-value" "10")}
  ;                                       {:element      "node3",
  ;                                        :type         "string",
  ;                                        :restrictions ("equal" "value2")})}}
  ;
  ;               '{:node "node1",
  ;                 :node1
  ;                       {:text       nil,
  ;                        :attributes {:att2 "2", :att1 "1"},
  ;                        :path       (:node1 :1 :children :root)}}
  ;               ())))

  ;(is (thrown? ExceptionInfo
  ;             (children-check
  ;               '{:element      "node1",
  ;                 :type         nil,
  ;                 :restrictions (),
  ;                 :complexType
  ;                               {:name nil,
  ;                                :elements
  ;                                      ({:element      "node2",
  ;                                        :type         "number",
  ;                                        :restrictions ("min-value" "0" "max-value" "10")}
  ;                                       {:element      "node3",
  ;                                        :type         "string",
  ;                                        :restrictions ("equal" "value2")})}}
  ;
  ;               '{:node "node1",
  ;                 :node1
  ;                       {:text       nil,
  ;                        :attributes {:att2 "2", :att1 "1"},
  ;                        :path       (:node1 :1 :children :root),
  ;                        :children
  ;                                    {:1
  ;                                     {:node "node2",
  ;                                      :node2
  ;                                            {:text       "1",
  ;                                             :attributes {},
  ;                                             :path       (:node2 :1 :children :node1 :1 :children :root)}},
  ;                                     :2
  ;                                     {:node "node3",
  ;                                      :node3
  ;                                            {:text       "value1",
  ;                                             :attributes {},
  ;                                             :path       (:node3 :2 :children :node1 :1 :children :root)}}
  ;                                     :3
  ;                                     {:node "node4",
  ;                                      :node3
  ;                                            {:text       "value2",
  ;                                             :attributes {},
  ;                                             :path       (:node3 :2 :children :node1 :1 :children :root)}}}}}
  ;               ())))

  ;(is (thrown? ExceptionInfo
  ;             (children-check
  ;               '{:element      "node1",
  ;                 :type         nil,
  ;                 :restrictions (),
  ;                 :complexType
  ;                               {:name nil,
  ;                                :elements
  ;                                      ({:element      "node2",
  ;                                        :type         "number",
  ;                                        :restrictions ("min-value" "0" "max-value" "10")}
  ;                                       {:element      "node3",
  ;                                        :type         "string",
  ;                                        :restrictions ("equal" "value2")}
  ;                                       {:element      "node4",
  ;                                        :type         "string",
  ;                                        :restrictions ("equal" "value2")})}}
  ;
  ;               '{:node "node1",
  ;                 :node1
  ;                       {:text       nil,
  ;                        :attributes {:att2 "2", :att1 "1"},
  ;                        :path       (:node1 :1 :children :root),
  ;                        :children
  ;                                    {:1
  ;                                     {:node "node2",
  ;                                      :node2
  ;                                            {:text       "1",
  ;                                             :attributes {},
  ;                                             :path       (:node2 :1 :children :node1 :1 :children :root)}},
  ;                                     :2
  ;                                     {:node "node3",
  ;                                      :node3
  ;                                            {:text       "value1",
  ;                                             :attributes {},
  ;                                             :path       (:node3 :2 :children :node1 :1 :children :root)}}}}}
  ;               ())))
  )

(def map-schema '{:element      "root",
                  :type         nil,
                  :restrictions (),
                  :complexType
                                {:name "type1",
                                 :elements
                                       ({:element      "node1",
                                         :type         nil,
                                         :restrictions (),
                                         :complexType
                                                       {:name nil,
                                                        :elements
                                                              ({:element      "node2",
                                                                :type         "number",
                                                                :restrictions ("min-value" "0" "max-value" "10")}
                                                               {:element      "node3",
                                                                :type         "string",
                                                                :restrictions ("equal" "value2")})}}
                                        {:element "node4", :type "string", :restrictions ()})}})

(def map-schema (schema/schema-tree->map (schema/schema-str->tree (str '(schema
                                                                          (complexTypes
                                                                            (complexType {:name "type1"}
                                                                                         (element {:name "node1"}
                                                                                                  (complexType
                                                                                                    (element {:name "node1" :type "number"}
                                                                                                             (restrictions
                                                                                                               (:min 0)
                                                                                                               (:max 10)))
                                                                                                    (element {:name "node2" :type "string" :minOccurs 0 :maxOccurs 10}
                                                                                                             (restrictions
                                                                                                               (:enum value1, value2)))))))

                                                                          (element {:name "root"}
                                                                                   (complexType {:name "type1"}
                                                                                                (element {:name "node1"}
                                                                                                         (complexType
                                                                                                           (element {:name "node2" :type "number" :minOccurs 0 :maxOccurs 10}
                                                                                                                    (restrictions
                                                                                                                      (:min 0)
                                                                                                                      (:max 10)))
                                                                                                           (element {:name "node3" :type "string" :minOccurs 0 :maxOccurs 10}
                                                                                                                    (restrictions
                                                                                                                      (:enum value1, value2))))))))))))
(def map-doc (doc/doc-tree->map (doc/doc-str->tree (str '(root
                                                           (node1 {:att1 1 :att2 2}
                                                                  (node2 "1")
                                                                  (node3 "value1")))))))
;(pprint/pprint (get-in map-schema [:schema :element]))


(deftest compare-element-node-test
  (is (empty? (compare-element-node (get-in map-schema [:schema :element]) map-doc ())))

  (is (thrown? ExceptionInfo (compare-element-node
                               '{:element      "node1",
                                 :type         nil,
                                 :restrictions (),
                                 :complexType
                                               {:name nil,
                                                :elements
                                                      ({:element      "node2",
                                                        :type         "number",
                                                        :restrictions ("min-value" "0" "max-value" "10")}
                                                       {:element      "node3",
                                                        :type         "string",
                                                        :restrictions ("equal" "value2")})}}

                               '{:name "type1",
                                 :elements
                                       ({:element      "node1",
                                         :type         nil,
                                         :restrictions ()}
                                        {:element "node4", :type "string", :restrictions ()})}
                               ()))))


(deftest doc-map-to-xml-map-test
  (is (= (doc-map-to-xml-map map-doc))))


(def stylesheet (str '(stylesheet
                        <root>
                        <tag1 att = "test" > (value-of "/*/node1") </tag1>
                        <tag2> (value-of "/*/node2") </tag2>
                        (to-xml "/*/node3")
                        <tag3> (to-xml "/*/node4") </tag3>
                        <tag4> "Hello!" </tag4>
                        <tag4 att = (value-of "/*/node1") >
                        <tag5> (value-of "/*/node2") </tag5>
                        </tag4>
                        </root>)))


(deftest doc-map-to-xml-map-test
  (is (= (transform-stylesheet (stylesheet/stylesheet-tree->map (stylesheet/stylesheet-str->tree stylesheet))
                               doc-map)
         '{:tag :root,
           :content
                [{:tag :tag1, :content [""], :attrs {:att "test"}}
                 {:tag :tag2, :content [""]}
                 {:tag :tag3, :content [{:tag :node4, :content ["value2"]}]}
                 {:tag :tag4, :content ["Hello!"]}
                 {:tag     :tag4,
                  :content [{:tag :tag5, :content [""]}],
                  :attrs
                           {:att
                            (:template "(" (:valueof "value-of" (:xpath "/*/node1")) ")")}}]})))


(def stl-map '{:stylesheet
               {:tag :root,
                :content
                     [{:tag :tag1,
                       :content
                            ["Hello!"],
                       :attrs
                            {:att1 "1",
                             :att2
                                   {:template
                                    {:value-of "//node4", :to-xml nil, :match nil, :match-xml []}}}}]}})

(def doc-map '{:node "root",
               :root
                     {:text       nil,
                      :attributes {},
                      :path       (:root),
                      :children
                                  {:1
                                   {:node "node1",
                                    :node1
                                          {:text       nil,
                                           :attributes {:att2 "2", :att1 "1"},
                                           :path       (:node1 :1 :children :root),
                                           :children
                                                       {:1
                                                        {:node "node2",
                                                         :node2
                                                               {:text       "1",
                                                                :attributes {},
                                                                :path       (:node2 :1 :children :node1 :1 :children :root)}},
                                                        :2
                                                        {:node "node3",
                                                         :node3
                                                               {:text       "value1",
                                                                :attributes {},
                                                                :path       (:node3 :2 :children :node1 :1 :children :root)}}}}},
                                   :2
                                   {:node "node4",
                                    :node4
                                          {:text       "value2",
                                           :attributes {},
                                           :path       (:node4 :2 :children :root)}}
                                   :3
                                   {:node "node5",
                                    :node4
                                          {:text       "value3",
                                           :attributes {},
                                           :path       (:node4 :2 :children :root)}}}}})

(deftest transform-stylesheet-test
  (is (= (transform-stylesheet stl-map doc-map))))

;(run-tests 'sexp-document.processor-test)

;(pprint/pprint (transform-stylesheet stl-map doc-map))
;(transform-stylesheet stl-map doc-map)
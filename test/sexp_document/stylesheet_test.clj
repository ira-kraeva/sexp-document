(ns sexp-document.stylesheet-test
  (:require [clojure.test :refer :all])
  (:require [clojure.pprint :as pprint])
  (:use [sexp-document.stylesheet-reader :as stylesheet])
  (:import (clojure.lang ExceptionInfo))
  )

(def stylesheet (str '(stylesheet
                        <root>
                        <tag1 att1 = "1" att2 = "2" > (value-of "/*/node1") </tag1>
                        <tag2> (value-of "/*/node2") </tag2>
                        (match "/*/node3"
                               <int_tag1> (value-of "./node4") </int_tag1>
                               <int_tag2> (to-xml "//node5") </int_tag2>
                               )
                        <tag3> (to-xml "/*/node6") </tag3>
                        <tag4> "Hello!" </tag4>
                        <tag5>
                        <tag6> (value-of "/*/node2") </tag6>
                        </tag5>
                        </root>)))

(def stl-tree '(:stylesheet
                 "("
                 "stylesheet"
                 (:xml
                   "<"
                   (:tag "root")
                   ">"
                   (:children
                     (:xml
                       "<"
                       (:tag "tag1")
                       (:attributes
                         (:attribute "att1" "=" "1")
                         (:attribute "att2" "=" "2"))
                       ">"
                       (:children
                         (:template "(" (:valueof "value-of" (:xpath "/*/node1")) ")"))
                       "</"
                       "tag1"
                       ">")
                     (:xml
                       "<"
                       (:tag "tag2")
                       ">"
                       (:children
                         (:template "(" (:valueof "value-of" (:xpath "/*/node2")) ")"))
                       "</"
                       "tag2"
                       ">")
                     (:template
                       "("
                       (:match "match" (:xpath "/*/node3"))
                       (:xml
                         "<"
                         (:tag "int_tag1")
                         ">"
                         (:children
                           (:template "(" (:valueof "value-of" (:xpath "./node4")) ")"))
                         "</"
                         "int_tag1"
                         ">")
                       (:xml
                         "<"
                         (:tag "int_tag2")
                         ">"
                         (:children
                           (:template "(" (:toxml "to-xml" (:xpath "//node5")) ")"))
                         "</"
                         "int_tag2"
                         ">")
                       ")")
                     (:xml
                       "<"
                       (:tag "tag3")
                       ">"
                       (:children
                         (:template "(" (:toxml "to-xml" (:xpath "/*/node6")) ")"))
                       "</"
                       "tag3"
                       ">")
                     (:xml "<" (:tag "tag4") ">" (:value "Hello!") "</" "tag4" ">")
                     (:xml
                       "<"
                       (:tag "tag5")
                       ">"
                       (:children
                         (:xml
                           "<"
                           (:tag "tag6")
                           ">"
                           (:children
                             (:template "(" (:valueof "value-of" (:xpath "/*/node2")) ")"))
                           "</"
                           "tag6"
                           ">"))
                       "</"
                       "tag5"
                       ">"))
                   "</"
                   "root"
                   ">")
                 ")"
                 "<EOF>"))

(def stl-map '{:stylesheet
               {:tag :root,
                :content
                     [{:template
                       {:value-of nil,
                        :to-xml   nil,
                        :match    "/*/node3",
                        :match-xml
                                  [{:tag :int_tag1,
                                    :content
                                         [{:template
                                           {:value-of  "./node4",
                                            :to-xml    nil,
                                            :match     nil,
                                            :match-xml []}}]}
                                   {:tag :int_tag2,
                                    :content
                                         [{:template
                                           {:value-of  nil,
                                            :to-xml    "//node5",
                                            :match     nil,
                                            :match-xml []}}]}]}}
                      {:tag   :tag1,
                       :content
                              [{:template
                                {:value-of "/*/node1", :to-xml nil, :match nil, :match-xml []}}],
                       :attrs {:att1 "1", :att2 "2"}}
                      {:tag :tag2,
                       :content
                            [{:template
                              {:value-of "/*/node2", :to-xml nil, :match nil, :match-xml []}}]}
                      {:tag :tag3,
                       :content
                            [{:template
                              {:value-of nil, :to-xml "/*/node6", :match nil, :match-xml []}}]}
                      {:tag :tag4, :content ["Hello!"]}
                      {:tag :tag5,
                       :content
                            [{:tag :tag6,
                              :content
                                   [{:template
                                     {:value-of  "/*/node2",
                                      :to-xml    nil,
                                      :match     nil,
                                      :match-xml []}}]}]}]}})

(deftest stylesheet-str->tree-test (is (= (stylesheet-str->tree stylesheet)
                                          stl-tree)))

;(deftest stylesheet-tree->map-test (is (= (stylesheet-tree->map stl-tree)
;                                          stl-map)))

;(pprint/pprint (stylesheet-tree->map (stylesheet-str->tree (slurp "../../resources/examples/s-stylesheet.txt"))))

(run-tests 'sexp-document.stylesheet-test)


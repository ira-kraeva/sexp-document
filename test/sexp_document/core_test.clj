(ns sexp-document.core-test
  (:require [clojure.test :refer :all])
  (:require [sexp-document.document-reader :as doc])
  (:require [sexp-document.path-reader :as path])
  (:use [sexp-document.core :as core])
  (:require [sexp-document.writer :as writer])
  (:require [sexp-document.processor :as processor])
  (:require [clojure.pprint :as pprint])
  (:require [sexp-document.schema-reader :as schema]
            [sexp-document.stylesheet-reader :as stylesheet])
  (:import (clojure.lang ExceptionInfo)))

(def doc (str '(root
                 (node1 {:att1 1 :att2 2}
                        (node2 "1")
                        (node3 "value1"))
                 (node4 "value2"))))

(deftest find-test
  (is (= (core/find "/root" doc)
         '(("root" ("node1" {:att2 "2", :att1 "1"} ("node2" "1") ("node3" "value1")) ("node4" "value2")))))

  (is (= (core/find "/root/node1" doc)
         '(("node1" {:att2 "2", :att1 "1"}
             ("node2" "1")
             ("node3" "value1")))))

  (is (= (core/find "/*" doc)
         '(("root"
             ("node1" {:att2 "2", :att1 "1"} ("node2" "1") ("node3" "value1"))
             ("node4" "value2")))))

  (is (= (core/find "//*" doc)
         '(("root"
             ("node1" {:att2 "2", :att1 "1"} ("node2" "1") ("node3" "value1"))
             ("node4" "value2"))
           ("node1" {:att2 "2", :att1 "1"} ("node2" "1") ("node3" "value1"))
           ("node2" "1")
           ("node3" "value1")
           ("node4" "value2"))))

  (is (= (core/find "//*/node2" doc)
         '(("node2" "1"))))

  (is (= (core/find "//*[text() = value2]" doc)
         '(("node4" "value2"))))

  (is (= (core/find "//*[text() > 0]" doc)
         '(("node2" "1")))))


(deftest update-test
  (is (= (update "/root/node1/node2" doc "New value")
         '("root"
            ("node1"
              {:att2 "2", :att1 "1"}
              ("node2" "New value")
              ("node3" "value1"))
            ("node4" "value2"))))

  (is (= (update "/root/node1/node7" doc "New value")
         '("root"
            ("node1"
              {:att2 "2", :att1 "1"}
              ("node2" "1")
              ("node3" "value1"))
            ("node4" "value2"))))

  (is (thrown? ExceptionInfo (update "/root/node1/node7" doc 1))))

(deftest add-test
  (is (= (add "/root/node1/node2" doc (str '(node96
                                              (node97 "100")
                                              (node98 {:att1 "1" :att2 "2"} "100"))))
         '("root"
            ("node1"
              {:att2 "2", :att1 "1"}
              ("node2" "1"
                ("node96"
                  ("node97" "100")
                  ("node98" "100" {:att1 "1" :att2 "2"})))
              ("node3" "value1"))
            ("node4" "value2"))))

  (is (= (add "/root/node1/node99" doc (str '(node96
                                               (node97 "100")
                                               (node98 {:att1 "1" :att2 "2"} "100"))))
         '("root"
            ("node1"
              {:att2 "2", :att1 "1"}
              ("node2" "1")
              ("node3" "value1"))
            ("node4" "value2")))))


(def schema (str '(schema
                    (complexTypes
                      (complexType {:name "type1"}
                                   (element {:name "node5"}
                                            (complexType
                                              (element {:name "node6" :type "number"}
                                                       (restrictions
                                                         (:min 0)
                                                         (:max 10)))
                                              (element {:name "node7" :type "string" :minOccurs 0 :maxOccurs 1}
                                                       (restrictions
                                                         (:enum value1, value2)))))))

                    (element {:name "root"}
                             (complexType
                               (element {:name "node1"}
                                        (complexType
                                          (element {:name "node2" :type "number" :minOccurs 0 :maxOccurs 10}
                                                   (restrictions
                                                     (:min 0)
                                                     (:max 10)))
                                          (element {:name "node3" :type "string" :minOccurs 0 :maxOccurs 10}
                                                   (restrictions
                                                     (:enum value1, value2)))))
                               (element {:name "node4"}
                                        (complexType {:name "type1"})))))))

(def doc1 (str '(root
                  (node1 {:att1 1 :att2 2}
                         (node2 "1")
                         (node3 "value1"))
                  (node4
                    (node5
                      (node6 "9")
                      (node7 "value1"))))))

(deftest validate-test
  (is (= (validate schema doc1)
         "Document is valid")))

(def stylesheet (str '(stylesheet
                        <root>
                        <tag1 att = "test" > (value-of "//node2") </tag1>
                        <tag2> (value-of "//node3") </tag2>
                        (to-xml "//node4")
                        <tag3> (to-xml "//node5") </tag3>
                        <tag4> "Hello!" </tag4>
                        <tag5>
                        <tag6> (value-of "//node7") </tag6>
                        <tag7> (to-xml "//node1") </tag7>
                        </tag5>
                        </root>)))

(def document (str '(root
                      (node1 {:att1 1 :att2 2}
                             (node2 "1")
                             (node3 "value1"))
                      (node4
                        (node5
                          (node6 "9")
                          (node7 "value1"))))))

(run-tests 'sexp-document.core-test)
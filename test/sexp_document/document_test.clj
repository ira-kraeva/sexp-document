(ns sexp-document.document-test
  (:require [clojure.test :refer :all])
  (:use [sexp-document.document-reader])
  (:require [clojure.pprint :as pprint]
            [sexp-document.document-reader :as doc])
  (:import (clojure.lang ExceptionInfo)))

(def doc-str (str '(root
                     (node1 {:att1 1 :att2 2}
                            (node2 "1")
                            (node3 "value1"))
                     (node4 "value2"))))

(deftest node-name-test
  (is (= (node-name '(:node "(" (:nodeName "node2")))
         "node2"))

  (is (= (node-name '(:node "(" (:nodeName "root")
                       (:node "(" (:nodeName "node1")
                         (:node "(" (:nodeName "node2") "1" ")")
                         (:node "(" (:nodeName "node3") "value1" ")") ")")
                       (:node "(" (:nodeName "node4") "value2" ")") ")"))
         "root"))

  (is (= (node-name '(:node "(" (:nodeName "node3") "value1" ")"))
         "node3"))

  (is (thrown? ExceptionInfo (node-name '(:node "(" "value1" ")")))))


(deftest text-test
  (is (nil? (text '(:node "(" (:nodeName "node2")))))

  (is (= (text '(:node "{" "}" "(" (:nodeName "node2") "value1" "<EOF>"))
         "value1"))

  (is (thrown? ExceptionInfo (text '(:node "{" "}" "(" (:nodeName "node2") "value1" "value2" "<EOF>")))))


(deftest attributes-test
  (is (= (attributes '(:node
                        "("
                        (:nodeName "node1")
                        (:attributes "{" "att1" "1" "att2" "2" "}")
                        (:node "(" (:nodeName "node2") "1" ")")
                        (:node "(" (:nodeName "node3") "value1" ")")
                        ")"))
         '{:att2 "2", :att1 "1"}))

  (is (= (attributes '(:node
                        "("
                        (:nodeName "node1")
                        (:node "(" (:nodeName "node2") "1" ")")
                        (:node "(" (:nodeName "node3") "value1" ")")
                        ")"))
         '{})))


(deftest doc-str->tree-test
  (is (= (doc-str->tree doc-str)
         '(:document
           (:node
             "("
             (:nodeName "root")
             (:node
               "("
               (:nodeName "node1")
               (:attributes "{" "att1" "1" "att2" "2" "}")
               (:node "(" (:nodeName "node2") "1" ")")
               (:node "(" (:nodeName "node3") "value1" ")")
               ")")
             (:node "(" (:nodeName "node4") "value2" ")")
             ")")
           "<EOF>")))

  (is (nil? (doc-str->tree "(root
                              (node1 {:att1 1 :att2 2}
                                (node2 1)
                                (node3 value1))
                                (node4 value2)"))))


(def doc-tree (doc-str->tree doc-str))
(def doc-map '{:node "root",
               :root
                     {:text nil,
                      :attributes {},
                      :path (:root),
                      :children
                      {:1
                       {:node "node1",
                        :node1
                              {:text nil,
                               :attributes {:att2 "2", :att1 "1"},
                               :path (:node1 :1 :children :root),
                               :children
                               {:1
                                {:node "node2",
                                 :node2
                                       {:text "1",
                                        :attributes {},
                                        :path (:node2 :1 :children :node1 :1 :children :root)}},
                                :2
                                {:node "node3",
                                 :node3
                                       {:text "value1",
                                        :attributes {},
                                        :path (:node3 :2 :children :node1 :1 :children :root)}}}}},
                       :2
                       {:node "node4",
                        :node4
                              {:text "value2",
                               :attributes {},
                               :path (:node4 :2 :children :root)}}}}})


(deftest doc-tree->map-test
  (is (= (doc-tree->map doc-tree) doc-map)))


;(pprint/pprint (doc-tree->map (doc-str->tree doc-str)))

(run-tests 'sexp-document.document-test)
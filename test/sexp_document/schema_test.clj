(ns sexp-document.schema-test
  (:require [clojure.test :refer :all])
  (:require [clojure.pprint :as pprint])
  (:use [sexp-document.schema-reader :as schema])
  (:import (clojure.lang ExceptionInfo))
  )


(def schema (str '(schema
                    (complexTypes
                      (complexType {:name "type1"}
                                   (element {:name "node1"}
                                            (complexType
                                              (element {:name "node1" :type "number"}
                                                       (restrictions
                                                         (:min 0)
                                                         (:max 10)))
                                              (element {:name "node2" :type "string" :minOccurs 0 :maxOccurs 10}
                                                       (restrictions
                                                         (:enum value1, value2)))))))

                    (element {:name "root"}
                             (complexType {:name "type1"}
                                          (element {:name "node3"}
                                                   (complexType
                                                     (element {:name "node4" :type "number" :minOccurs 0 :maxOccurs 10}
                                                              (restrictions
                                                                (:min 0)
                                                                (:max 10)))
                                                     (element {:name "node5" :type "string" :minOccurs 0 :maxOccurs 10}
                                                              (restrictions
                                                                (:enum value1, value2))))))))))

;(pprint/pprint (schema-tree->map (schema-str->tree (slurp "../../resources/examples/s-schema.txt"))))


(def element1 '(:element
                 "("
                 "element"
                 "{"
                 ":name"
                 (:elementName "node1")
                 "}"
                 (:complexType
                   "("
                   "complexType"
                   "{"
                   ":name"
                   (:typeName "type1")
                   "}"
                   (:element
                     "("
                     "element"
                     "{"
                     ":name"
                     (:elementName "node2")
                     ":type"
                     (:elementType "number")
                     "}"
                     (:restritions
                       "(restrictions"
                       (:restriction "(" ":min-value" "0" ")")
                       (:restriction "(" ":max-value" "10" ")")
                       ")")
                     ")")
                   (:element
                     "("
                     "element"
                     "{"
                     ":name"
                     (:elementName "node3")
                     ":type"
                     (:elementType "string")
                     "}"
                     (:restritions
                       "(restrictions"
                       (:restriction "(" ":enum" "value2" ")")
                       ")")
                     ")")
                   ")")
                 ")"))


(deftest element-name-test
  (is (= (schema/element-name element1)
         "node1"))

  (is (thrown? ExceptionInfo (schema/element-name '(:element
                                                     "("
                                                     "element"
                                                     "{"
                                                     ":name"
                                                     (:elementName "node1")
                                                     (:elementName "node2")
                                                     "}"
                                                     (:complexType
                                                       "("
                                                       "complexType"
                                                       "{"
                                                       ":name"
                                                       (:typeName "type1")
                                                       "}"
                                                       (:element
                                                         "("
                                                         "element"
                                                         "{"
                                                         ":name"
                                                         (:elementName "node2")
                                                         ":type"
                                                         (:elementType "number")
                                                         "}"
                                                         (:restritions
                                                           "(restrictions"
                                                           (:restriction "(" "min-value" "0" ")")
                                                           (:restriction "(" "max-value" "10" ")")
                                                           ")")
                                                         ")")
                                                       (:element
                                                         "("
                                                         "element"
                                                         "{"
                                                         ":name"
                                                         (:elementName "node3")
                                                         ":type"
                                                         (:elementType "string")
                                                         "}"
                                                         (:restritions
                                                           "(restrictions"
                                                           (:restriction "(" "equal" "value2" ")")
                                                           ")")
                                                         ")")
                                                       ")")
                                                     ")")))))


(deftest element-type-test
  (is (= (schema/element-type '(:element
                                 "("
                                 "element"
                                 "{"
                                 ":name"
                                 (:elementName "node4")
                                 ":type"
                                 (:elementType "string")
                                 "}"
                                 ")")))
      "string"))


(deftest restrictions-test
  (is (= (schema/restrictions '(:element
                                 "("
                                 "element"
                                 "{"
                                 ":name"
                                 (:elementName "node2")
                                 ":type"
                                 (:elementType "number")
                                 "}"
                                 (:restrictions
                                   "(restrictions"
                                   (:restriction "(" "min-value" "0" ")")
                                   (:restriction "(" "max-value" "10" ")")
                                   ")")
                                 ")"))
         '((:restriction "(" "min-value" "0" ")") (:restriction "(" "max-value" "10" ")")))))

(deftest complex-types-test
  (is (= (schema/complex-types (schema-str->tree schema)) '((:complexType
                                                              "("
                                                              "complexType"
                                                              "{:name"
                                                              (:typeName "type1")
                                                              "}"
                                                              (:element
                                                                "("
                                                                "element"
                                                                "{:name"
                                                                (:elementName "node1")
                                                                "}"
                                                                (:complexType
                                                                  "("
                                                                  "complexType"
                                                                  (:element
                                                                    "("
                                                                    "element"
                                                                    "{:name"
                                                                    (:elementName "node1")
                                                                    ":type"
                                                                    (:elementType "number")
                                                                    "}"
                                                                    (:restrictions
                                                                      "("
                                                                      "restrictions"
                                                                      "("
                                                                      (:restriction ":min" "0")
                                                                      ")"
                                                                      "("
                                                                      (:restriction ":max" "10")
                                                                      ")"
                                                                      ")")
                                                                    ")")
                                                                  (:element
                                                                    "("
                                                                    "element"
                                                                    "{:name"
                                                                    (:elementName "node2")
                                                                    ":type"
                                                                    (:elementType "string")
                                                                    ":minOccurs"
                                                                    (:minOccurs "0")
                                                                    ":maxOccurs"
                                                                    (:maxOccurs "10")
                                                                    "}"
                                                                    (:restrictions
                                                                      "("
                                                                      "restrictions"
                                                                      "("
                                                                      (:restriction ":enum" "value1" "value2")
                                                                      ")"
                                                                      ")")
                                                                    ")")
                                                                  ")")
                                                                ")")
                                                              ")")))))

;(pprint/pprint (schema/complex-types (schema-str->tree schema)))

(def complex-type1 '(:complexType
                      "("
                      "complexType"
                      "{"
                      ":name"
                      (:typeName "type1")
                      "}"
                      (:element
                        "("
                        "element"
                        "{"
                        ":name"
                        (:elementName "node2")
                        ":type"
                        (:elementType "number")
                        "}"
                        (:restritions
                          "(restrictions"
                          (:restriction "(" "min-value" "0" ")")
                          (:restriction "(" "max-value" "10" ")")
                          ")")
                        ")")
                      (:element
                        "("
                        "element"
                        "{"
                        ":name"
                        (:elementName "node3")
                        ":type"
                        (:elementType "string")
                        "}"
                        (:restritions
                          "(restrictions"
                          (:restriction "(" "equal" "value2" ")")
                          ")")
                        ")")
                      ")"))


(deftest complex-type-test
  (is (= (schema/complex-type complex-type1))
      "type1"))

(deftest elements-test
  (is (= (schema/elements complex-type1)
         '((:element
             "("
             "element"
             "{"
             ":name"
             (:elementName "node2")
             ":type"
             (:elementType "number")
             "}"
             (:restritions
               "(restrictions"
               (:restriction "(" "min-value" "0" ")")
               (:restriction "(" "max-value" "10" ")")
               ")")
             ")")
           (:element
             "("
             "element"
             "{"
             ":name"
             (:elementName "node3")
             ":type"
             (:elementType "string")
             "}"
             (:restritions
               "(restrictions"
               (:restriction "(" "equal" "value2" ")")
               ")")
             ")"))))

  (is (= (schema/elements '(:complexType
                             "("
                             "complexType"
                             ")"))
         ())))


(deftest schema-tree->map-test (is (= (schema/schema-tree->map (schema-str->tree schema))
                                      '{:schema
                                        {:complex-types
                                         ({:name "type1",
                                           :elements
                                                 ({:element      "node1",
                                                   :type         nil,
                                                   :min-occurs   nil,
                                                   :max-occurs   nil,
                                                   :restrictions (),
                                                   :complexType
                                                                 {:name nil,
                                                                  :elements
                                                                        ({:element      "node1",
                                                                          :type         "number",
                                                                          :min-occurs   nil,
                                                                          :max-occurs   nil,
                                                                          :restrictions ({::min ("0")} {::max ("10")}),
                                                                          :complexType  nil}
                                                                         {:element      "node2",
                                                                          :type         "string",
                                                                          :min-occurs   "0",
                                                                          :max-occurs   "10",
                                                                          :restrictions ({::enum ("value1" "value2")}),
                                                                          :complexType  nil})}})}),
                                         :element
                                         {:element      "root",
                                          :type         nil,
                                          :min-occurs   nil,
                                          :max-occurs   nil,
                                          :restrictions (),
                                          :complexType
                                                        {:name "type1",
                                                         :elements
                                                               ({:element      "node3",
                                                                 :type         nil,
                                                                 :min-occurs   nil,
                                                                 :max-occurs   nil,
                                                                 :restrictions (),
                                                                 :complexType
                                                                               {:name nil,
                                                                                :elements
                                                                                      ({:element      "node4",
                                                                                        :type         "number",
                                                                                        :min-occurs   "0",
                                                                                        :max-occurs   "10",
                                                                                        :restrictions ({::min ("0")} {::max ("10")}),
                                                                                        :complexType  nil}
                                                                                       {:element      "node5",
                                                                                        :type         "string",
                                                                                        :min-occurs   "0",
                                                                                        :max-occurs   "10",
                                                                                        :restrictions ({::enum ("value1" "value2")}),
                                                                                        :complexType  nil})}})}}}})))

;(pprint/pprint (schema/schema-tree->map (schema-str->tree schema)))

;(run-tests 'sexp-document.schema-test)
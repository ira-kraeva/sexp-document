grammar document;

document
   : node+ EOF
   ;

node
   : '(' nodeName (SYMBOL | NUMBER) ')'
   | '(' nodeName attributes (SYMBOL | NUMBER) ')'
   | '(' nodeName node+ ')'
   | '(' nodeName attributes ')'
   | '(' nodeName attributes node+ ')'
   | '(' nodeName (SYMBOL | NUMBER) node+ ')'
   | '(' nodeName attributes (SYMBOL | NUMBER) node+ ')'
   ;

nodeName
   : SYMBOL
   ;

attributes
   : '{' ( SYMBOL (SYMBOL | NUMBER))+  '}'
   ;

WHITESPACE
   : (' ' | '\n' | '\t' | '\r' | ','| ';' | '"' | ':')+ -> skip
   ;

NUMBER
   : ('+' | '-')? (DIGIT)+ ('.' (DIGIT)+)?
   ;

SYMBOL
   : (SYMBOL_START | DIGIT)+
   ;

fragment SYMBOL_START
   : ('a' .. 'z')
   | ('A' .. 'Z')
   | '-'
   ;

fragment DIGIT
   : ('0' .. '9')
   ;
grammar stylesheet;

stylesheet
   : '(' 'stylesheet' xml* template* ')' EOF
   ;

xml
   : '<' tag (attributes)?'>' (value | children) '</' SYMBOL '>'
   ;

template
   : '(' (toxml | valueof | match) xml* template*')'
   ;

tag
   : SYMBOL
   ;

attributes
   : attribute+
   ;

attribute
   :  SYMBOL '=' (SYMBOL | NUMBER | template)
   ;

value
   : (SYMBOL | NUMBER)*
   ;

children
   : ((xml+ template*) | (template+ xml*))+
   ;

toxml
   : 'to-xml' xpath
   ;

valueof
   : 'value-of' xpath
   ;

match
   : 'match' xpath
   ;

xpath
   : SYMBOL
   ;

WHITESPACE
   : (' ' | '\n' | '\t' | '\r' | ','| ';' | '"' | ':')+ -> skip
   ;

NUMBER
   : ('+' | '-')? (DIGIT)+ ('.' (DIGIT)+)?
   ;

SYMBOL
   : (SYMBOL_START | DIGIT)+
   ;

fragment SYMBOL_START
   : ('a' .. 'z')
   | ('A' .. 'Z')
   | '-'
   | '/'
   | '*'
   | '@'
   | '['
   | ']'
   | '!'
   | '_'
   | '.'
   ;

fragment DIGIT
   : ('0' .. '9')
   ;
grammar path;

path
  : absolutePath
  | relativePath
  ;

relativePath
  : '//' step (relativePath)?
  | '//' step (absolutePath)?
  ;

absolutePath
  : '/' step (relativePath)?
  | '/' step (absolutePath)?
  ;

step
  : nodeTest (predicate)?
  ;

nodeTest
  :  SYMBOL
  | '*'
  ;

predicate
  : '[' expression ']'
  ;

expression
  : (function | attribute) operator value
  ;

function
  : SYMBOL '()'
  ;

operator
  : '='
  | '<'
  | '>'
  ;

value
  : NUMBER
  | SYMBOL
  ;

attribute
  : '@' SYMBOL
  ;

STRING
   : '"' ('\\' . | ~ ('\\' | '"'))* '"'
  ;

WHITESPACE
   : (' ' | '\n' | '\t' | '\r' | ','| ';' )+ -> skip
   ;

NUMBER
   : ('+' | '-')? (DIGIT)+ ('.' (DIGIT)+)?
   ;

SYMBOL
   : SYMBOL_START (SYMBOL_START | DIGIT)*
   ;

fragment SYMBOL_START
   : ('a' .. 'z')
   | ('A' .. 'Z')
   ;

fragment DIGIT
   : ('0' .. '9')
   ;

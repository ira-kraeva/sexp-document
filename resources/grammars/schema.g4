grammar schema;

schema
   : '(' 'schema' complexTypes? element+ ')' EOF
   ;

element
   : '(' 'element' '{:name' elementName (':type' elementType)? (':minOccurs' minOccurs)? (':maxOccurs' maxOccurs)? '}' (restrictions | complexType)? ')'
   ;

restrictions
   : '(' 'restrictions' ('(' restriction+ ')')+  ')'
   ;

restriction
   : SYMBOL (SYMBOL | NUMBER)+
   ;

complexTypes
   : '(' 'complexTypes' complexType+ ')'
   ;

complexType
   : '(' 'complexType' ( '{:name' typeName '}' )? element* ')'
   ;

elementName
   : SYMBOL
   ;

elementType
   : SYMBOL
   ;

typeName
   : SYMBOL
   ;

minOccurs
   : NUMBER
   ;

maxOccurs
   : NUMBER
   ;

WHITESPACE
   : (' ' | '\n' | '\t' | '\r' | ','| ';' | '"')+ -> skip
   ;

NUMBER
   : ('+' | '-')? (DIGIT)+ ('.' (DIGIT)+)?
   ;

SYMBOL
   : (SYMBOL_START | DIGIT)+
   ;

fragment SYMBOL_START
   : ('a' .. 'z')
   | ('A' .. 'Z')
   | '-'
   | ':'
   ;

fragment DIGIT
   : ('0' .. '9')
   ;
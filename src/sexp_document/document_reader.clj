(ns sexp-document.document-reader
  (:require [clj-antlr.core :as antlr])
  (:require [clojure.pprint :as pprint])
  (:import (clj_antlr ParseError))
  )

(defn node-name
  "Возвращает имя ноды в виде строки. Если передан документ, то имя первой дочерней ноды."

  [expr]
  (let [coll (filter
               #(= :nodeName (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         (throw
                           (ex-info "Can't define " {:expression expr})))))

(defn text
  "Возвращает текст ноды в виде строки. Если передан документ, то возвращает nil."

  [expr]
  (let [coll (filter
               #(and
                  (not (keyword? %))
                  (not (seq? %))
                  (not= "(" %)
                  (not= ")" %)
                  (not= "{" %)
                  (not= "}" %)
                  (not= "<EOF>" %))
               expr)]
    (if
      (<= (count coll) 1) (first coll)
                          (throw
                            (ex-info "More than one value in the node" {:node coll})))))

(defn children-nodes
  "Возвращает список дочерних нод переданной ноды или документа."

  [expr]
  (filter #(= :node (first %)) (rest expr)))

(defn attributes
  "Возвращает атрибуты ноды в виде hash map"

  [expr]
  (if (= :nodeName (first expr))
    (attributes (children-nodes expr))
    (let [coll (filter
                 #(= :attributes (first %))
                 (filter #(seq? %) expr))
          coll->map (fn [x]
                      (apply hash-map (mapcat
                                        #(list (keyword (first %)) (second %))
                                        (partition 2 (filter #(and (not (keyword? %))
                                                                   (not= "{" %)
                                                                   (not= "}" %))
                                                             (first x))))))]
      (coll->map coll))))

(defn doc-str->tree
  "Парсит документ, полученный в виде строки (для чтения файла рекомендуется использовать slurp),
  с помощью antlr4:
   - antlr4 - https://github.com/antlr/antlr4,
   - бандл для clojure - https://github.com/aphyr/clj-antlr.
  Используется грамматика document.g4.
  Возвращает дерево нод."

  [doc-string]
  (let [antlr-object (antlr/parser "../../resources/grammars/document.g4")]
    (try
      (antlr-object doc-string)
      (catch ParseError e (pprint/pprint @e)))))

(defn doc-tree->map
  "Возвращает hash map с данными документа со структурой:
    :node - имя ноды в виде строки,
    :text - текст ноды в виде строки, может быть nil,
    :attributes - атрибуты ноды в виде hash map, может быть nil,
    :path - путь до ноды в виде списка, шаги представлены в виде ключей с названием нод, хранится в обратном порядке (нода -> корневой элемент)
    :children - дочерние ноды в виде hash map, каждая дочерняя нода имеет ключ, указывающий на номер ноды по порядку."

  [expr & [path]]
  (if (= :document (first expr))
    (doc-tree->map (first (children-nodes expr)))
    (let [m {:node                      (node-name expr)
             (keyword (node-name expr)) {:text       (text expr)
                                         :attributes (attributes expr)
                                         :path       (if (nil? path) (cons (keyword (node-name expr)) ())
                                                                     (cons (keyword (node-name expr)) path))}}
          m (if
              (not (empty? (children-nodes expr)))
              (assoc-in
                m
                [(keyword (node-name expr)) :children]
                (reduce
                  merge
                  (map
                    #(assoc
                       {}
                       (keyword (str %))
                       (into {} (doc-tree->map (nth (children-nodes expr) (- % 1))
                                               (if (nil? path)
                                                 (cons (keyword (str %))
                                                       (cons
                                                         :children
                                                         (cons (keyword (node-name expr)) ())))
                                                 (cons (keyword (str %))
                                                       (cons
                                                         :children
                                                         (cons (keyword (node-name expr)) path)))))))
                    (range 1 (inc (count (children-nodes expr)))))))
              m)
          ]
      m)))
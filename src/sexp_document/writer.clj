(ns sexp-document.writer

  (:import (javax.xml.transform.stream StreamResult StreamSource)
           (java.io StringReader StringWriter)
           (javax.xml.transform TransformerFactory OutputKeys)))

(defn doc-map->tree
  "Преобразует документ, представленный в виде hash map, в s-документ."

  [document-map]
  (let [name (get document-map :node)
        attributes (get-in document-map [(keyword name) :attributes])
        text (get-in document-map [(keyword name) :text])]

    (filter
      #(and
         (not (nil? %))
         (not= () %)
         (not= {} %))
      (reduce conj (map
                     #(doc-map->tree (second %))
                     (get-in document-map [(keyword name) :children]))
              (list attributes text name)))))


(defn element-to-xml [e]
  (if (instance? String e)
    e
    (concat
      (str "<"  (name (:tag e)) )
      (when (:attrs e)
        (mapcat
          #(str " " (name (key %)) "='" (val %) "'")
          (:attrs e)))
      (if (:content e)
        (concat ">"
                (mapcat
                  #(element-to-xml %)
                  (:content e))
                (str "</" (name (:tag e)) ">"))
        "/>"))))

(defn document-to-xml [x]
  (apply str (element-to-xml x)))

(defn ppxml [xml]
  (let [in (javax.xml.transform.stream.StreamSource.
             (java.io.StringReader. xml))
        writer (java.io.StringWriter.)
        out (javax.xml.transform.stream.StreamResult. writer)
        transformer (.newTransformer
                      (javax.xml.transform.TransformerFactory/newInstance))]
    (.setOutputProperty transformer
                        javax.xml.transform.OutputKeys/INDENT "yes")
    (.setOutputProperty transformer
                        "{http://xml.apache.org/xslt}indent-amount" "2")
    (.setOutputProperty transformer
                        javax.xml.transform.OutputKeys/METHOD "xml")
    (.transform transformer in out)
    (-> out .getWriter .toString)))
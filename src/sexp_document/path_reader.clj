(ns sexp-document.path-reader
  (:require [clj-antlr.core :as antlr])
  (:require [clojure.pprint :as pprint])
  (:import (clj_antlr ParseError))
  )

(defn a-r-path
  "Возвращает абсолютный (/) или относительный путь (//) в текущем шаге."

  [xpath]
  (first
    (filter
      #(or
         (= :absolutePath (first %))
         (= :relativePath (first %)))
      (filter #(seq? %) xpath))))

(defn step
  "Возвращает шаг в текущей ноде в виде выражения."

  [xpath]
  (if
    (= :path (first xpath))
    (step (a-r-path xpath))

    (let [step (nth xpath 2)]
      (when (= :step (first step))
        (nth step 1)))))

(defn test-node
  "Возвращает выражение test node для текущей ноды в виде строки."

  [xpath]
  (nth (step xpath) 1))

(defn predicate
  "Возвращает предикат в текущей ноды в виде списка."

  [xpath]
  (filter #(seq? %)
          (first (filter
                   #(= :predicate (first %))
                   (filter #(seq? %)
                           (first (filter #(seq? %) xpath)))))))


(defn expression
  "Возвращает выражение в переданном выражение (должен передаваться предикат) в виде выражения."

  [expression]
  (let [coll (filter #(= :expression (first %)) (filter #(seq? %) expression))]
    (if (empty? coll)
      nil
      (rest (first coll)))))

(defn function
  "Возвращает название функции из переданного выражения в виде строки."

  [expr]
  (let [coll (filter
               #(= :function (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn attribute
  "Возвращает название функции из переданного выражения в виде строки."

  [expr]
  (let [coll (filter
               #(= :attribute (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn operator
  "Возвращает логический оператор из переданного выражения в виде строки."

  [expr]
  (let [coll (filter
               #(= :operator (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn value
  "Возвращает значение из переданного выражения в виде строки."

  [expr]
  (let [coll (filter
               #(= :value (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn attribute
  "Возвращает название проверяемого атрибута из переданного выражения в виде строки."

  [expr]
  (let [coll (filter
               #(= :attribute (first %))
               (filter #(seq? %) expr))]
    (if (= (count coll) 1)
      (first (filter
               #(and (not (keyword? %)) (not= "@" %))
               (first coll)))
      nil)))

(defn next-step
  "Возвращает следующий шаг в виде выражения."

  [xpath]
  (cond
    (or
      (= :absolutePath (first xpath))
      (= :relativePath (first xpath)))
    (a-r-path xpath)

    (= :path (first xpath))
    (next-step (a-r-path xpath))

    :else (throw
            (ex-info "Given path is not absolute or relative path" {:type-err :wrong-expression}))))

(defn path-str->tree
  "Парсит xpath, полученный в виде строки,
   с помощью antlr4:
   - antlr4 - https://github.com/antlr/antlr4,
   - бандл для clojure - https://github.com/aphyr/clj-antlr.
   Используется грамматика path.g4.
   Возвращает дерево нод."

  [string]
  (let [s-path (antlr/parser "../../resources/grammars/path.g4")]
    (try
      (s-path string)
      (catch ParseError e (pprint/pprint @e)))))

(defn path-tree->map
  "Возвращает hash map с данными xpath со структурой:
    :context - контекст шага, поддерживаются child и descendant-or-self,
    :node-test - node-test в виде строки, может быть nil,
    :predicate - предикат в виде hash map, может быть nil, содержит:
      :function - название функции,
      :attribute - название проверяемого атрибута, не поддерживается,
      :operator - логический оператор, поддерживается =, >, <,
      :value - проверяемое значение, должно быть строкой.
    :path - путь до ноды в виде списка, шаги представлены в виде ключей с названием нод, хранится в обратном порядке (нода -> корневой элемент)
    :children - дочерние ноды в виде hash map, каждая дочерняя нода имеет ключ, указывающий на номер ноды по порядку."

  [expr]
  (if (= :path (first expr))
    (path-tree->map (a-r-path expr))

    (let [m {:context   (cond
                          (= :absolutePath (first expr)) "child"
                          (= :relativePath (first expr)) "descendant-or-self"
                          :else "other")
             :node-test (test-node expr)
             :predicate {:function  (function (expression (predicate expr)))
                         :attribute (attribute (expression (predicate expr)))
                         :operator  (operator (expression (predicate expr)))
                         :value     (value (expression (predicate expr)))}
             }
          m (if (not (nil? (a-r-path expr)))
              (assoc m :next-step (path-tree->map (a-r-path expr)))
              m)]
      m)))
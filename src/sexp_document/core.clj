(ns sexp-document.core
  (:require [sexp-document.document-reader :as doc])
  (:require [sexp-document.schema-reader :as schema])
  (:require [sexp-document.path-reader :as path])
  (:require [sexp-document.writer :as writer])
  (:require [sexp-document.stylesheet-reader :as stylesheet])
  (:use [sexp-document.processor])
  )


(defn find
  "Выполняет поиск ноды в документе по xpath.
   Документ должен быть передан в виде строки (для чтения рекомендуется использовать slurp).
   Xpath должен быть передан в виде строки.
   Возвращает список найденный нод."
  [xpath document]

  (map writer/doc-map->tree (compare-xpath-node
                              (path/path-tree->map (path/path-str->tree xpath))
                              (doc/doc-tree->map (doc/doc-str->tree document)))))

(defn update
  "Выполняет изменение всех нод в документе, заданных с помощью xpath.
   Документ должен быть передан в виде строки (для чтения рекомендуется использовать slurp).
   Xpath должен быть передан в виде строки.
   Value должно быть строкой.
   Возвращает измененный документ. Если ни одна нода не соответствует xpath, то возвращает неизмененный документ."

  [xpath document value]
  (if (string? value)
    (writer/doc-map->tree (reduce
                            (fn [dcm pth]
                              (let [path (reverse (cons :text pth))]
                                (if (nil? (get-in dcm path))
                                  dcm
                                  (assoc-in dcm path value))))
                            (doc/doc-tree->map (doc/doc-str->tree document))
                            (map #(get-in % [(keyword (get % :node)) :path]) (compare-xpath-node
                                                                               (path/path-tree->map (path/path-str->tree xpath))
                                                                               (doc/doc-tree->map (doc/doc-str->tree document))))))
    (throw
      (ex-info "Value must be a string." {:value value}))))


(defn add
  "Добавляет переданную ноды в качестве дочерней для всех нод в документе, заданных с помощью xpath.
   Документ и нода должны быть переданы в виде строки (для чтения рекомендуется использовать slurp).
   Xpath должен быть передан в виде строки.
   Возвращает измененный документ. Если ни одна нода не соответствует xpath, то возвращает неизмененный документ."

  [xpath document node]

  (writer/doc-map->tree (reduce
                          (fn [dcm pth]
                            (let [path (reverse (cons
                                                  (keyword (str (inc (count (get dcm :children)))))
                                                  (cons :children pth)))]
                              (assoc-in dcm path (doc/doc-tree->map (doc/doc-str->tree node)))))
                          (doc/doc-tree->map (doc/doc-str->tree document))
                          (map #(get-in % [(keyword (get % :node)) :path]) (compare-xpath-node
                                                                             (path/path-tree->map (path/path-str->tree xpath))
                                                                             (doc/doc-tree->map (doc/doc-str->tree document)))))))

(defn validate
  "Выполняет валиацию документа по схеме.
   Документ и схема должны быть переданы в виде строки (для чтения рекомендуется использовать slurp).
   Возвращает строку 'Document is valid', если документ соответствует схеме.
   Если не соответствует, то кидает исключение с описанием ошибки."

  [schema document]
  (let [map-schema (schema/schema-tree->map (schema/schema-str->tree schema))
        types (get-in map-schema [:schema :complex-types])]
    (when (empty? (compare-element-node
                    (get-in map-schema [:schema :element])
                    (doc/doc-tree->map (doc/doc-str->tree document)) types))
      "Document is valid")))

(defn transform-to-xml
  "Преобразует S-документ в XML-документ в соответствии с шаблоном.
   Документ и шаблон должны быть переданы в виде строки (для чтения рекомендуется использовать slurp)."

  [stylesheet document]
  (writer/ppxml (writer/document-to-xml (transform-stylesheet
                                          (stylesheet/stylesheet-tree->map (stylesheet/stylesheet-str->tree stylesheet))
                                          (doc/doc-tree->map (doc/doc-str->tree document))))))
(ns sexp-document.stylesheet-reader
  (:require [clj-antlr.core :as antlr])
  (:require [clojure.pprint :as pprint])
  (:import (clj_antlr ParseError))
  )

(defn stylesheet-str->tree
  "Парсит шаблон, полученный в виде строки (для чтения файла рекомендуется использовать slurp),
  с помощью antlr4:
   - antlr4 - https://github.com/antlr/antlr4,
   - бандл для clojure - https://github.com/aphyr/clj-antlr.
  Используется грамматика stylesheet.g4.
  Возвращает дерево элементов шаблона."

  [stylesheet-string]
  (let [antlr-object (antlr/parser "../../resources/grammars/stylesheet.g4")]
    (try
      (antlr-object stylesheet-string)
      (catch ParseError e (pprint/pprint @e)))))

(defn get-node
  [node-key expr]
  (filter
    #(= node-key (first %))
    (filter #(seq? %) expr)))

(defn stylesheet-tree->map
  "Возвращает hash map с данными шаблона со структурой:
    :stylesheet - корневой элемент шаблона,
      :xml - xml-документ,
        :tag - тег,
        :attributes - аттрибуты тега,
        :children - дочерние теги,
      :template - шаблон,
        :value-of - xpath, значение первой ноды, найденной по этому пути, будет использовано для заполнения тега XML-документе,
        :to-xml - xpath, первая нода, найденная по этому пути, будет добавлена в качестве дочерних тегов для тега XML-документе,"

  [expr]
  (cond
    (= :stylesheet (first expr))
    (let [m {:stylesheet (first (map #(stylesheet-tree->map %) (concat (get-node :xml expr) (get-node :template expr))))}]
      m)

    (= :xml (first expr))
    (let [attributes (first (get-node :attributes expr))
          children (first (get-node :children expr))
          value (first (get-node :value expr))
          m {:tag     (keyword (second (first (get-node :tag expr))))
             :content (cond
                        (not (nil? children))
                        (into [] (map #(stylesheet-tree->map %) (concat
                                                                  (get-node :template
                                                                            children)
                                                                  (get-node :xml
                                                                            children))))

                        (not (nil? value))
                        [(apply str (rest value))])
             }]
      (if (not (nil? attributes))
        (assoc m :attrs (when
                          (not (nil? attributes))
                          (into {} (mapcat #(stylesheet-tree->map %) (get-node
                                                                       :attribute
                                                                       (first (get-node :attributes expr)))))))
        m))

    (= :template (first expr))
    (let [m {:template {:value-of (second (first (get-node :xpath
                                                           (first (get-node :valueof expr)))))

                        :to-xml   (second (first (get-node :xpath
                                                           (first (get-node :toxml expr)))))
                        :match    (second (first (get-node :xpath
                                                           (first (get-node :match expr)))))
                        :match-xml  (into [] (map stylesheet-tree->map (get-node :xml expr)))}}]
      m)

    (= :attribute (first expr))
    (let [value (nth expr 3)
          m (if (seq? value)
              {(keyword (second expr)) (stylesheet-tree->map value)}
              {(keyword (second expr)) value})]
      m)))
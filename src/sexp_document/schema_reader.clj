(ns sexp-document.schema-reader
  (:require [clj-antlr.core :as antlr])
  (:require [clojure.pprint :as pprint])
  (:import (clj_antlr ParseError))
  )

(defn element
  "Возвращает элемент."

  [expr]
  (let [coll (filter
               #(= :element (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (first coll)
                         nil)))

(defn element-name
  "Возвращает имя переданного элемента в виде строки."

  [expr]
  (let [coll (filter
               #(= :elementName (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         (throw
                           (ex-info "Can't define element name." {:element expr})))))

(defn element-type
  "Возвращает тип переданного элемента в виде строки."

  [expr]
  (let [coll (filter
               #(= :elementType (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn min-occurs
  "Возвращает минимальное количество вхождений элемента в виде строки."

  [expr]
  (let [coll (filter
               #(= :minOccurs (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn max-occurs
  "Возвращает аксимальное количество вхождений элемента в виде строки."

  [expr]
  (let [coll (filter
               #(= :maxOccurs (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))

(defn complex-types
  "Возвращает список комплексных типов, использующхся в схеме. Если комплексные типы не заданы для схемы, то nil."

  [expr]
  (let [coll (filter
               #(= :complexTypes (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (filter #(seq? %) (first coll))
                         nil)))

(defn complex-type
  "Возвращает комплексный тип элемента в виде выражени. Если элемент простой, то nil."

  [expr]
  (let [coll (filter
               #(= :complexType (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (first coll)
                         nil)))

(defn type-name
  "Возвращает название комплексного типа."

  [expr]
  (let [coll (filter
               #(= :typeName (first %))
               (filter #(seq? %) expr))]
    (if
      (= (count coll) 1) (second (first coll))
                         nil)))


(defn restrictions
  "Возвращает ограничения переданного элемента в виде коллекции.
   Если ограничения не заданы, тo возвращает nil."

  [expression]
  (let [coll (filter
               #(= :restrictions (first %))
               (filter #(seq? %) expression))]
    (if
      (= (count coll) 1) (filter
                           #(and (not= "(restrictions" %)
                                 (not= "(" %)
                                 (not= ")" %))
                           (rest (first coll)))
                         ())))

(defn elements
  "Возвращает список элементов в комплексном типе."

  [expression]
  (filter
    #(= :element (first %))
    (filter #(seq? %) expression)))


(defn schema-str->tree
  "Парсит схему, полученный в виде строки (для чтения файла рекомендуется использовать slurp),
  с помощью antlr4:
   - antlr4 - https://github.com/antlr/antlr4,
   - бандл для clojure - https://github.com/aphyr/clj-antlr.
  Используется грамматика schema.g4.
  Возвращает дерево элементов."

  [string]
  {:post [(= :schema (first %))]}
  (let [antlr-object (antlr/parser "../../resources/grammars/schema.g4")]
    (try
      (antlr-object string)
      (catch ParseError e (pprint/pprint @e)))))


(defn schema-tree->map
  "Возвращает hash map с данными схемы со структурой:
    :element - имя элемента в виде строки,
    :type - тип элемента, поддерживаются string и number, может быть nil,
    :restrictions - органичения значения элемента, поддерживаются enum, min-value, max-value,
    :complexType - комплексный тип элемента."

  [expr]
  (cond
    (= :schema (first expr))
    (let [m {:schema {:complex-types (map #(schema-tree->map %) (complex-types expr))
                      :element       (schema-tree->map (element expr))}}]
      m)

    (= :element (first expr))
    (let [m {:element      (element-name expr)
             :type         (element-type expr)
             :min-occurs   (min-occurs expr)
             :max-occurs   (max-occurs expr)
             :restrictions (map
                             #(assoc
                                {}
                                (first (filter (fn [x] (not (keyword? x))) %))
                                (rest (filter (fn [x] (not (keyword? x))) %)))
                             (filter #(seq? %) (restrictions expr)))
             :complexType  (schema-tree->map (complex-type expr))}]
      m)

    (= :complexType (first expr))
    (let [m {:name     (type-name expr)
             :elements (map schema-tree->map (elements expr))}]
      m)))
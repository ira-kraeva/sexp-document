(ns sexp-document.processor
  (:require [sexp-document.path-reader :as path])
  (:require [clojure.pprint :as pprint])
  )

(defn node-test-check
  "Используется при поиске ноды по xpath.
   Возвращает true, если нода соответствует node test, указанному в шаге xpath."

  [xpath node]
  (let
    [node-test (get xpath :node-test)
     node-name (get node :node)]
    (true? (or
             (= (str node-test) :*)
             (= (str node-test) ":*")
             (= (str node-test) "*")
             (= node-test node-name)))))

(defn parse-number
  "Преобразает переданную строку в число."

  [string]
  (Integer/parseInt string))

(defn is-number?
  "Вовзаращает true, если переданная строка содержит число."

  [string]
  (= string (re-find (re-matcher #"\d+" string))))

(defn compare-value
  "Возвращает true, если выражение (value1 operator value2) логически верно.
   Поддерживаются значения operator =, >, <."

  [operator value1 value2]
  (cond
    (= operator "=")
    (= value1 value2)

    (= operator ">") (if (and (not (nil? value2)) (not (nil? value1)))
                       (if (and (is-number? value2) (is-number? value1))
                         (> (parse-number value1)
                            (parse-number value2))
                         false)
                       false)

    (= operator "<") (if (and (not (nil? value2)) (not (nil? value1)))
                       (if (and (is-number? value2) (is-number? value1))
                         (< (parse-number value1) (parse-number value2))
                         false)
                       false)

    :else (throw
            (ex-info "Any other operator except =, >, < is not supported: " {:operator operator})))

  )

(defn predicate-check
  "Используется при поиске ноды по xpath.
   Возвращает true, если нода соответствует предикату, указанному в шаге xpath."

  [xpath node]
  (let
    [node-name (keyword (get node :node))
     function (get-in xpath [:predicate :function])
     attribute-test (get-in xpath [:predicate :attribute])
     node-attribute (get-in node [node-name :attributes (keyword attribute-test)])
     operator (get-in xpath [:predicate :operator])
     value (get-in xpath [:predicate :value])
     node-text (get-in node [node-name :text])]

    (cond
      (and (nil? function) (nil? attribute-test)) true

      (not (nil? function))
      (cond
        (= function "text")
        (compare-value operator node-text value)
        :else (throw
                (ex-info "Any other function except text is not supported: " {:function function})))

      (not (nil? attribute-test))
      (compare-value operator node-attribute value))))

(defn descendant-self-nodes
  "Возвращает всех потомков переданной ноды (не только дочерние ноды, всех потомков) в том числе переданную ноду в виде списка."

  [node]
  (let [node-name (keyword (get node :node))
        children (get-in node [node-name :children])]
    (cons node (mapcat (fn [ch] (descendant-self-nodes (second ch))) children))))

(defn descendant-nodes
  "Возвращает всех потомков переданной ноды (не только дочерние ноды, всех потомков) в виде списка."

  [node & [not-first]]
  (let [node-name (keyword (get node :node))
        children (get-in node [node-name :children])]
    (filter #(not (nil? %))
            (cons
              (if (nil? not-first) nil node)
              (reduce
                concat
                ()
                (map (fn [ch] (descendant-nodes (second ch) 1)) children))))))


(defn node-check
  "Используется при поиске ноды по xpath.
   Возвращает true, если нода соответствует шагу xpath (node test и пердикату)."

  [xpath node]
  (let [next-step (get xpath :next-step)
        xpath-last-step (nil? next-step)
        node-children (get-in node [(keyword (get node :node)) :children])
        leaf (nil? node-children)
        next-step-context (get next-step :context)]

    (when (node-test-check xpath node)
      (when (predicate-check xpath node)
        (cond
          (true? xpath-last-step) node

          (and (false? xpath-last-step) (false? leaf))
          (cond
            (= next-step-context "child")
            (map #(node-check next-step (second %)) node-children)

            (= next-step-context "descendant-or-self")
            (map #(node-check next-step %) (descendant-nodes node))

            :else (throw
                    (ex-info "Any other context except child and descendant-or-self is not supported: " {:context next-step-context}))))))))

(defn compare-xpath-node
  "Используется при поиске ноды по xpath.
   Возвращает список нод, соответствующих переданному xpath.
   Проверка выполняется для переданной ноды и ее потомков."

  [xpath node]
  (let [context (get xpath :context)]
    (filter #(not (nil? %))
            (flatten (cond
                       (= context "child")
                       (let [result (node-check xpath node)] (if (map? result) (list result)
                                                                               result))

                       (= context "descendant-or-self")
                       (map #(node-check xpath %) (descendant-self-nodes node))

                       :else (throw
                               (ex-info "Any other context except child and descendant-or-self is not supported: " {:context context})))))))


(defn name-check
  "Используется при валидации документа по схеме.
   Возвращает true, если название ноды в документе соответствует названию элемента в схеме.
   Если не соответствует, то кидает исключение с описанием ошибки."

  [element node]
  (let
    [node-name (get node :node)
     element-name (get element :element)]
    (if (= element-name node-name)
      true

      (throw
        (ex-info "Name check failed: " {:element-name element-name :node-name node-name})))))

(defn type-check
  "Используется при валидации документа по схеме.
   Возвращает true, если тип ноды в документе соответствует типу элемента в схеме.
   Если не соответствует, то кидает исключение с описанием ошибки."

  [element node]
  (let
    [node-name (get node :node)
     element-type (get element :type)
     node-text (get-in node [(keyword node-name) :text])]

    (if (not (nil? element-type))
      (cond (= element-type "string")
            (if (string? node-text)
              true
              (throw
                (ex-info "Text of node has to be a string" {:node node-name :node-text node-text})))

            (= element-type "number")
            (if
              (= node-text (re-find (re-matcher #"\d+" node-text)))
              true
              (throw
                (ex-info "Text of node has to be a number" {:node node-name :node-text node-text}))))
      true)))


(defn restrictions-check
  "Используется при валидации документа по схеме.
   Возвращает true, если значение переданной ноды в документе соответствуют ограничениям, заданным для элемента в схеме.
   Поддерживаются следующие ограничения:
   - enum - значение должно быть равно одному из значений в списке возможных,
   - min - значение больше или равно указанному, если значение не число, то возвращает false,
   - max - значение меньше или равно указанному, если значение не число, то возвращает false.
   Если не соответствуют, то кидает исключение с описанием ошибки."

  [element node]
  (let
    [restrictions (get element :restrictions)
     node-name (get node :node)
     node-text (get-in node [(keyword node-name) :text])
     restriction-check
     (fn [restriction]

       (let [type (first (first restriction))
             value (second (first restriction))]

         (cond
           (= type ":enum")
           (if (true?
                 (some true? (map #(= node-text %) value)))
             true
             (throw
               (ex-info "Node-text is wrong: " {:node node-name :node-text node-text :restriction-type type :restriction-value value})))

           (= type ":min")
           (if (and
                 (= 1 (count value))
                 (is-number? (first value))
                 (is-number? node-text)
                 (true? (> (parse-number node-text)
                           (parse-number (first value)))))
             true
             (throw
               (ex-info "Node-text is wrong: " {:node node-name :node-text node-text :restriction-type type :restriction-value value})))

           (= type ":max")
           (if (and
                 (= 1 (count value))
                 (is-number? (first value))
                 (is-number? node-text)
                 (true? (< (parse-number node-text)
                           (parse-number (first value)))))
             true
             (throw
               (ex-info "Node-text is wrong: " {:node node-name :node-text node-text :restriction-type type :restriction-value value})))

           :else (throw
                   (ex-info "Any other restriction except enum, min and max is not supported: " {:restriction type})))))]

    (if (empty? restrictions)
      true
      (every? true? (map restriction-check restrictions)))))


(defn children-check
  "Используется при валидации документа по схеме.
   Возвращает true, если дочерние ноды переданной ноды в документе соответствуют описанию в схеме
   (проверяется наличие дочерних нод и их количество).
   Если не соответствуют, то кидает исключение с описанием ошибки."

  [element node complex-types]
  (let
    [node-name (get node :node)
     complex-type (get element :complexType)
     children (get-in node [(keyword node-name) :children])
     complex-type-name (get-in element [:complexType :name])
     elements (if (empty? (get complex-type :elements))
                (get (first (filter #(= (get % :name) complex-type-name) complex-types)) :elements)
                (get complex-type :elements))]

    (cond
      (and (nil? complex-type) (nil? children))
      true

      (and (not (nil? complex-type)) (nil? children))
      (throw
        (ex-info "Node has to be complex: " {:node node-name}))

      (and (nil? complex-type) (not (nil? children)))
      (throw
        (ex-info "Node has to be simple: " {:node node-name}))

      ;(and (not (nil? complex-type)) (not (nil? children)))
      ;(if (= (count elements) (count children))
      ;  true
      ;  (throw
      ;    (ex-info "Node has wrong amount of children: " {:node node-name :elements (count elements) :children (count children)})))
      )))

(defn occurs-check

  [element node complex-types]
  (let [node-name (get node :node)
        complex-type (get element :complexType)
        children (get-in node [(keyword node-name) :children])
        complex-type-name (get-in element [:complexType :name])
        elements (if (empty? (get complex-type :elements))
                   (get (first (filter #(= (get % :name) complex-type-name) complex-types)) :elements)
                   (get complex-type :elements))]

    (if (every? true? (map
                        (fn [element]

                          (let [min-occurs (get element :min-occurs)
                                max-occurs (get element :max-occurs)]

                            (cond
                              (and (nil? min-occurs) (nil? max-occurs))
                              true

                              (and (not (nil? min-occurs)) (nil? max-occurs))
                              (>=
                                (count (filter #(= (get (second %) :node) (get element :element)) children))
                                (parse-number min-occurs))

                              (and (nil? min-occurs) (not (nil? max-occurs)))
                              (<=
                                (count (filter #(= (get (second %) :node) (get element :element)) children))
                                (parse-number max-occurs))

                              (and (not (nil? min-occurs)) (not (nil? max-occurs)))
                              (and
                                (>=
                                  (count (filter #(= (get (second %) :node) (get element :element)) children))
                                  (parse-number min-occurs))
                                (<=
                                  (count (filter #(= (get (second %) :node) (get element :element)) children))
                                  (parse-number max-occurs))))))
                        elements))
      true
      (throw
        (ex-info "One or more of children nodes have wrong amount of occurrences: " {:node node :element element})))))

(defn compare-element-node
  "Используется при валидации документа по схеме.
   Возвращает пустой список, если нода в документе соответствует схеме.
   Проверка выполняется для переданной ноды и всех ее потомков."

  [element node complex-types]

  (let [node-name (get node :node)
        complex-type (get element :complexType)
        complex-type-name (get-in element [:complexType :name])

        elements (if (empty? (get complex-type :elements))
                   (get (first (filter #(= (get % :name) complex-type-name) complex-types)) :elements)
                   (get complex-type :elements))
        children (get-in node [(keyword node-name) :children])]

    (reduce
      concat
      ()
      (when (name-check element node)
        (when (type-check element node)
          (when (restrictions-check element node)
            (when (children-check element node complex-types)
              (when (occurs-check element node complex-types)
                (map #(compare-element-node %1 %2 complex-types)
                     elements
                     (map #(second %) children))))))))))

(defn doc-map-to-xml-map
  [doc-map]

  (let [tag (keyword (get doc-map :node))
        attrs (get-in doc-map [tag :attributes])
        children (get-in doc-map [(keyword tag) :children])
        m {:tag tag}]

    ;(print "\nNEXT STEP\n")
    ;(print (not (nil? content)))

    (if (not (nil? attrs))
      (assoc m :attrs (get-in doc-map [tag :attributes]))
      m)

    (if (not (nil? children))
      (assoc m :content (into []
                              (filter #(not (empty? %))
                                      (map
                                        #(doc-map-to-xml-map (second %))
                                        children))))
      (assoc m :content [(get-in doc-map [tag :text])]))))




(defn transform-stylesheet
  [stylesheet document & [parent-tag]]

  (let [stlsht (get stylesheet :stylesheet)
        tag (get stylesheet :tag)
        transform-template (fn [template]

                             (let [value-of (get template :value-of)
                                   to-xml (get template :to-xml)
                                   match (get template :match)
                                   match-xml (get template :match-xml)]

                               ;(print value-of)

                               (cond
                                 (not (nil? value-of))
                                 (let [node (first (compare-xpath-node (path/path-tree->map (path/path-str->tree value-of)) document))]
                                   ;(print "\nValue of\n")
                                   ;(pprint/pprint value-of)
                                   ;(pprint/pprint document)
                                   (str (get-in node [(keyword (get node :node)) :text])))

                                 (not (nil? to-xml))
                                 (let [nodes (compare-xpath-node (path/path-tree->map (path/path-str->tree to-xml)) document)]
                                   ;(print "\nTo XML\n")
                                   ;(pprint/pprint to-xml)
                                   ;(pprint/pprint document)
                                   ;(pprint/pprint nodes)
                                   (map #(doc-map-to-xml-map %) nodes))

                                 (not (nil? match))
                                 (let [nodes (when (not (nil? match))
                                               (compare-xpath-node (path/path-tree->map (path/path-str->tree match)) document))]
                                   ;(print "\nMatch\n")
                                   ;(pprint/pprint match)
                                   ;(pprint/pprint document)
                                   ;(pprint/pprint nodes)
                                   ;(pprint/pprint match-xml)
                                   (map
                                     (fn [m]
                                       (map
                                         (fn [n] (transform-stylesheet m n))
                                         nodes))
                                     match-xml))

                                 :else (throw
                                         (ex-info "Any other option is nor supported:" {:template template})))))]

    ;(print "\nNEXT\n")
    ;(pprint/pprint document)
    ;(pprint/pprint stylesheet)
    ;(pprint/pprint tag)
    ;(pprint/pprint template)
    ;(pprint/pprint match-xml)

    (cond
      (not (nil? stlsht))
      (transform-stylesheet stlsht document)

      (not (nil? tag))
      (let [content (get stylesheet :content)
            attrs (get stylesheet :attrs)]

        ;(print (first content))

        (-> stylesheet
            (assoc :content (if
                              (not (string? (first content)))
                              (vec (flatten (let [tags (filter #(not (nil? (get % :tag))) content)
                                                templates (filter #(not (nil? %)) (map #(get % :template) content))]

                                            (list
                                              (if (not (empty? tags))
                                                (map #(transform-stylesheet % document tag) tags)
                                                ())

                                              (if (not (empty? templates))
                                                (map #(transform-template %) templates)
                                                ())))))
                              content))
            (assoc :attrs (into {}
                                (map
                                  #(if (map? (second %))
                                     {(keyword (first %)) (transform-template (get (second %) :template))}
                                     {(keyword (first %)) (second %)})
                                  attrs)))))

      :else stylesheet)))
(ns sexp-document.demo
  (:use [sexp-document.core])
  (:use [sexp-document.document-reader])
  (:require [clojure.pprint :as pprint]
            [clojure.pprint :as pprint]
            [clojure.pprint :as pprint]))

; пример s-документа
(def s-doc (str '(root
                   (node1 {:att1 1 :att2 2}
                          (node3 "1")
                          (node4 "value1")
                          (node5
                            (node6 "3")
                            (node7 "value2")
                            (node8
                              (node9 "4")
                              (node10
                                (node13 "value3")
                                (node14 "value4"))
                              (node11
                                (node15 "5"))
                              (node12 "3"))))
                   (node2 "2"))))

;--------------------------
;--------------------------

; поиск в s-документе
; по абсолютному пути

;(pprint/pprint (find "/root/node1/node5/node8" s-doc))

;(("node8"
;  ("node9" "4")
;  ("node10" ("node13" "value3") ("node14" "value4"))
;  ("node11" ("node15" "5"))
;  ("node12" "3")))

;--------------------------

; c использование * для обозначения любой ноды

;(pprint/pprint (find "/root/*/node5/*" s-doc))

;(("node6" "3")
; ("node7" "value2")
; ("node8"
;  ("node9" "4")
;  ("node10" ("node13" "value3") ("node14" "value4"))
;  ("node11" ("node15" "5"))
;  ("node12" "3")))

;--------------------------

; по относительному пути

;(pprint/pprint (find "/*//node3" s-doc))

;(("node3" "value1"))

;--------------------------

; c использованием предикатов
; по значению ноды
;(pprint/pprint (find "//*[text()=2]" s-doc))

;(("node4" "2"))

; по значению атрибута ноды
;(pprint/pprint (find "//*[@att1=1]" s-doc))

;(("node1"
;   {:att2 "2", :att1 "1"}
;   ("node3" "1")
;   ("node4" "value1")
;   ("node5"
;     ("node6" "3")
;     ("node7" "value2")
;     ("node8"
;       ("node9" "4")
;       ("node10" ("node13" "value3") ("node14" "value4"))
;       ("node11" ("node15" "5"))
;       ("node12" "3")))))

;--------------------------
;--------------------------

; изменение s-документа
; меняется значение на указанное у всех найденных нод в документе

;(pprint/pprint (update "/root/node1/*" s-doc "New value"))

;("root"
; ("node1"
;  {:att2 "2", :att1 "1"}
;  ("node3" "New value")
;  ("node4" "New value")
;  ("node5"
;   ("node6" "3")
;   ("node7" "value2")
;   ("node8"
;    ("node9" "4")
;    ("node10" ("node13" "value3") ("node14" "value4"))
;    ("node11" ("node15" "5"))
;    ("node12" "3"))))
; ("node2" "2"))

;--------------------------
; если нет подходящих нод, то возвращается неизмененный документ

;(pprint/pprint (update "/root/node5/*" s-doc "New value"))

;("root"
; ("node1"
;  {:att2 "2", :att1 "1"}
;  ("node3" "1")
;  ("node4" "value1")
;  ("node5"
;   ("node6" "3")
;   ("node7" "value2")
;   ("node8"
;    ("node9" "4")
;    ("node10" ("node13" "value3") ("node14" "value4"))
;    ("node11" ("node15" "5"))
;    ("node12" "3"))))
; ("node2" "2"))

;--------------------------
; добавляется дочерняя нода в качестве дочерней для всех найденных нод в документе

;(pprint/pprint (add "/root/node2" s-doc (str '(node96
;                                                      (node97 "100")
;                                                      (node98 {:att1 "1" :att2 "2"} "100")))))

;("root"
; ("node1"
;  {:att2 "2", :att1 "1"}
;  ("node3" "1")
;  ("node4" "value1")
;  ("node5"
;   ("node6" "3")
;   ("node7" "value2")
;   ("node8"
;    ("node9" "4")
;    ("node10" ("node13" "value3") ("node14" "value4"))
;    ("node11" ("node15" "5"))
;    ("node12" "3"))))
; ("node2"
;  "2"
;  ("node96" ("node97" "100") ("node98" "100" {:att2 "2", :att1 "1"}))))

;--------------------------
;--------------------------

; пример схемы для s-документа

(def s-doc (str '(root
                   (node1 {:att1 1 :att2 2}
                          (node2 "2")
                          (node3 "value1"))
                   (node4
                     (node5
                       (node6 "9")
                       (node7 "value1") (node7 "value1"))))))

(def s-schema (str '(schema
                      (complexTypes
                        (complexType {:name "type1"}
                                     (element {:name "node5"}
                                              (complexType
                                                (element {:name "node6" :type "number"}
                                                         (restrictions
                                                           (:min 0)
                                                           (:max 10)))
                                                (element {:name "node7" :type "string" :minOccurs 0 :maxOccurs 2}
                                                         (restrictions
                                                           (:enum value1, value2)))))))

                      (element {:name "root"}
                               (complexType
                                 (element {:name "node1"}
                                          (complexType
                                            (element {:name "node2" :type "number" :minOccurs 0 :maxOccurs 10}
                                                     (restrictions
                                                       (:min 1)
                                                       (:max 10)))
                                            (element {:name "node3" :type "string" :minOccurs 0 :maxOccurs 10}
                                                     (restrictions
                                                       (:enum value1, value2)))))
                                 (element {:name "node4"}
                                          (complexType {:name "type1"})))))))

;--------------------------
; валидация s-документа по схеме
; если документ валиден, то возвращается сообщение "Document is valid"

;(print (validate s-schema s-doc))

; Document is valid

;--------------------------
; если в докумете одна из нод называется не так как в схеме, кидается исключение
; в примере node2 вместо node7

(def s-doc-name-check (str '(root
                              (node1 {:att1 1 :att2 2}
                                     (node2 "1")
                                     (node3 "value1"))
                              (node4
                                (node5
                                  (node6 "9")
                                  (node2 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-name-check))

; clojure.lang.ExceptionInfo: Name check failed:  {:element-name "node7", :node-name "node2"}

;--------------------------
; если в докумете значение одной из нод имеет тип, отличный от указанного в схеме, кидается исключение
; в примере node6 имеет значение value1, в то время как в схеме для нее указан тип number
; здесь тип - примитив строка или число

(def s-doc-type-check (str '(root
                              (node1 {:att1 1 :att2 2}
                                     (node2 "1")
                                     (node3 "value1"))
                              (node4
                                (node5
                                  (node6 "value1")
                                  (node7 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-type-check))

; clojure.lang.ExceptionInfo: Text of node has to be a number {:node "node6", :node-text "value1"}

;--------------------------
; если в докумете значение одной из нод отличется от возможных, заданных для нее в схеме, кидается исключение
; в примере в схеме node7 можем имет значения value1 или value2, в документе value3

(def s-doc-restriction-enum (str '(root
                                    (node1 {:att1 1 :att2 2}
                                           (node2 "1")
                                           (node3 "value1"))
                                    (node4
                                      (node5
                                        (node6 "9")
                                        (node7 "value3"))))))

;(pprint/pprint (try (validate s-schema s-doc-restriction-enum)))

; clojure.lang.ExceptionInfo: Node-text is wrong:
; {:node "node7", :node-text "value3", :restriction-type ":enum", :restriction-value ("value1" "value2")}

;--------------------------
; если в докумете значение одной из нод меньше минимального, заданного для нее в схеме, кидается исключение
; в примере в схеме node2 должно быть значение больше 1, в документе 0

(def s-doc-restriction-min (str '(root
                                   (node1 {:att1 1 :att2 2}
                                          (node2 "0")
                                          (node3 "value1"))
                                   (node4
                                     (node5
                                       (node6 "9")
                                       (node7 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-restriction-min))

;clojure.lang.ExceptionInfo: Node-text is wrong:
; {:node "node2", :node-text "0", :restriction-type :min, :restriction-value ("1")}

;--------------------------
; если в докумете значение одной из нод меньше минимального, заданного для нее в схеме, кидается исключение
; в примере в схеме node2 должно быть значение меньше 10, в документе 11

(def s-doc-restriction-max (str '(root
                                   (node1 {:att1 1 :att2 2}
                                          (node2 "11")
                                          (node3 "value1"))
                                   (node4
                                     (node5
                                       (node6 "9")
                                       (node7 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-restriction-max))

;clojure.lang.ExceptionInfo: Node-text is wrong:
; {:node "node2", :node-text "11", :restriction-type :max, :restriction-value ("10")}

;--------------------------
; если в докумете нода, для которой в схеме задан комплексный тип с дочерними элементами, нет дочерних нод, кидается исключение
; в примере в схеме node4 должна иметь дочерние ноды node2 и node3

(def s-doc-no-children (str '(root
                               (node1 {:att1 1 :att2 2}
                                      (node2 "2")
                                      (node3 "value1"))
                               (node4 "1"))))

;(pprint/pprint (validate s-schema s-doc-no-children))

;clojure.lang.ExceptionInfo: Node has to be complex:  {:node "node4"}

;--------------------------
; если в докумете нода, для которой в схеме задан простой тип, есть дочерние ноды, кидается исключение
; в примере в схеме node2 не должна иметь дочерную ноду strange-node

(def s-doc-not-complex-type (str '(root
                                    (node1 {:att1 1 :att2 2}
                                           (node2 "9"
                                                  (strange-node "0"))
                                           (node3 "value2"))
                                    (node4
                                      (node5
                                        (node6 "9")
                                        (node7 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-not-complex-type))

;clojure.lang.ExceptionInfo: Node has to be simple:  {:node "node2"}

;--------------------------
; если в докумете нода, для которой в схеме указан комплексный тип с n дочерних элементов,
; есть m дочерних нод (n != m), кидается исключение
; в примере в схеме node1 должна иметь 2 дочерние ноды, в документе у нее 3 дочерние ноды

(def s-doc-wrong-amount-of-children (str '(root
                                            (node1 {:att1 1 :att2 2}
                                                   (node2 "9")
                                                   (node3 "value2")
                                                   (strange-node "0"))
                                            (node4
                                              (node5
                                                (node6 "9")
                                                (node7 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-wrong-amount-of-children))

;clojure.lang.ExceptionInfo: Node has wrong amount of children:  {:elements 2, :children 3}

;--------------------------
; если в докумете нода не соответствует указанному для нее в схеме комплексному типу, определенного в схеме, кидается исключение
; в примере в схеме node4 должна иметь комплексный тип type1 с 2 дочерними нодами, в документе у нее нет дочерних нод

(def s-doc-foreign-complex-type (str '(root
                                        (node1 {:att1 1 :att2 2}
                                               (node2 "1")
                                               (node3 "value1"))
                                        (node4
                                          (node5
                                            (node6 "9")
                                            (node7 "value1"))))))

;(pprint/pprint (validate s-schema s-doc-foreign-complex-type))

;clojure.lang.ExceptionInfo: Node has to be complex:  {:node "node4"}

;--------------------------
; если в схеме количество вхождений ноды меньше минимального, определенного в схеме, кидается исключение
; в примере node7 по схеме должна иметь минимум 2 вхождение, в документе - 1 вхождение

(def s-schema-min-occurs (str '(schema
                                 (complexTypes
                                   (complexType {:name "type1"}
                                                (element {:name "node5"}
                                                         (complexType
                                                           (element {:name "node6" :type "number"}
                                                                    (restrictions
                                                                      (:min 0)
                                                                      (:max 10)))
                                                           (element {:name "node7" :type "string" :minOccurs 2 :maxOccurs 1}
                                                                    (restrictions
                                                                      (:enum value1, value2)))))))

                                 (element {:name "root"}
                                          (complexType
                                            (element {:name "node1"}
                                                     (complexType
                                                       (element {:name "node2" :type "number" :minOccurs 0 :maxOccurs 10}
                                                                (restrictions
                                                                  (:min 1)
                                                                  (:max 10)))
                                                       (element {:name "node3" :type "string" :minOccurs 0 :maxOccurs 10}
                                                                (restrictions
                                                                  (:enum value1, value2)))))
                                            (element {:name "node4"}
                                                     (complexType {:name "type1"})))))))

(def s-doc-min-occurs (str '(root
                              (node1 {:att1 1 :att2 2}
                                     (node2 "2")
                                     (node3 "value1"))
                              (node4
                                (node5
                                  (node6 "9")
                                  (node7 "value1"))))))

;(pprint/pprint (validate s-schema-min-occurs s-doc-min-occurs))

;clojure.lang.ExceptionInfo: One or more of children nodes have wrong amount of occurrences

;--------------------------
;--------------------------
; Преобразование S-документа в XML-документ по шаблону
; Задаем шаблон match c xpath. Шаблон может содержать теги и другие шаблоны.
; В XML-документ будет добавлен шаблон для каждой ноды в S-документе, найденном по xpath.

(def stylesheet (str '(stylesheet
                        <root>
                        (match "//node1"
                               <node> (to-xml "/node1") </node>
                               <value> (value-of "/node1") </value>)
                        </root>)))

(def document (str '(root
                      (node1 "value1")
                      (node1 "value2")
                      (node1 "value3"))))

;(print (transform-to-xml stylesheet document))

;<?xml version="1.0" encoding="UTF-8"?>
; <root>
;  <node>
;    <node1>value1</node1>
;  </node>
;  <node>
;    <node1>value2</node1>
;  </node>
;  <node>
;    <node1>value3</node1>
;  </node>
;  <value>value1</value>
;  <value>value2</value>
;  <value>value3</value>
;</root>

;--------------------------
; Задаем шаблон to-xml c xpath
; В XML-документ будет добавлены все ноды в S-документе, найденном по xpath, преобразованные в XML.

(def stylesheet (str '(stylesheet
                        <root>
                        (to-xml "/root/node1")
                        </root>)))

(def document (str '(root
                      (node1
                        (node2 "value1")
                        (node3 "value2")))))

;(print (transform-to-xml stylesheet document))

;<?xml version="1.0" encoding="UTF-8"?>
; <root>
;  <node1>
;    <node2>value1</node2>
;    <node3>value2</node3>
;  </node1>
;</root>

;--------------------------
; Задаем шаблон value-of c xpath.
; В тег XML-документа будет добавлено значение ноды в S-документе, найденной по xpath, преобразованные в XML.
; Если в S-документе найдено более одной ноды, то будет использовано значение первой найденной ноды.
; Если нода в S-документе не найдена, то будет использована пустая строка.

(def stylesheet (str '(stylesheet
                        <root>
                        <node2>
                        (value-of "//node2")
                        </node2>
                        </root>)))

(def document (str '(root
                      (node1
                        (node2 "value1")
                        (node3 "value2")))))

;(print (transform-to-xml stylesheet document))

;<?xml version="1.0" encoding="UTF-8"?>
; <root>
;  <node2>value1</node2>
;</root>

;--------------------------
; Шаблон value-of также может быть использован для добавления значений в атрибуты XML-документа

(def stylesheet (str '(stylesheet
                        <root>
                        <node1 att1 = (value-of "//node2") att2 = "value2" >
                        <node2> "Hello!" </node2>
                        </node2>
                        </root>)))

(def document (str '(root
                      (node1
                        (node2 "value1")
                        (node3 "value2")))))

;(print (transform-to-xml stylesheet document))

;<?xml version="1.0" encoding="UTF-8"?>
; <root>
;  <node1 att1="value1" att2="value2">
;    <node2>Hello!</node2>
;  </node1>
;</root>

;--------------------------
; Сложный пример

(def stylesheet (str '(stylesheet
                        <root>
                        (match "//node1"
                               <node2> "Hello!"</node2>
                               <node1 att1 = (value-of "//node2") att2 = "Attribute!" >
                               (match "//node3"
                                      <nodes4> (match "//node4"
                                                      <node4> (value-of "//node4") </node4>)
                                      </nodes4>)
                               (to-xml "//node5")
                               </node1>)
                        </root>)))

(def document (str '(root
                      (node1 {:att1 1 :att2 2}
                             (node2 "1")
                             (node3
                               (node4 "value1") (node4 "value2") (node4 "value3")
                               (node5 "value4"))
                             (node6 "value5"))
                      (node1 {:att1 2 :att2 3}
                             (node2 "2")
                             (node3
                               (node4 "value4") (node4 "value5") (node4 "value6")
                               (node5 "value7"))
                             (node6 "value8")))))

(print (transform-to-xml stylesheet document))

;<?xml version="1.0" encoding="UTF-8"?><root>
;  <node2>Hello!</node2>
;  <node1 att1="1" att2="Attribute!">
;    <nodes4>
;      <node4>value1</node4>
;      <node4>value2</node4>
;      <node4>value3</node4>
;    </nodes4>
;    <node5>value4</node5>
;  </node1>
;  <node2>Hello!</node2>
;  <node1 att1="2" att2="Attribute!">
;    <nodes4>
;      <node4>value4</node4>
;      <node4>value5</node4>
;      <node4>value6</node4>
;    </nodes4>
;    <node5>value7</node5>
;  </node1>
;</root>
